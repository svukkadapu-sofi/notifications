#!/bin/sh

set -e

# Using vault / envconsul to supply environment.
if [ ! -z "${VAULT_ADDR}" ]; then
  exec envconsul -consul-addr $NODE_IP:8500 -config /data/sofi/bin/config.hcl -once "$@"
else
  exec "$@"
fi
