# REQUIRED ENVIRONMENT VARIABLES
#    VAULT_ADDR  - e.g. http://active.vault.service.consul:8200/
#    VAULT_TOKEN
#    NODE_IP
#
# USAGE:  envconsul -consul $NODE_IP:8500 -config='/config/config.hcl' java -Dvariable.runtime.declarations ... -jar ${JARFILE} 2>&1

max_stale = "10m"
sanitize = true
upcase = true

exec {
    kill_timeout = "5s"
    splay = "5s"
}

kill_signal = "SIGHUP"

consul {
  retry {
    enabled = true
    attempts = 3
    backoff = "5s"
    max_backoff = "30s"
  }
}

vault {
  renew_token = false
  retry {
    enabled = true
    attempts = 3
    backoff = "5s"
    max_backoff = "30s"
  }
}

prefix {
  path = "config/notifications"
  format = "notifications_{{key}}"
}

# DB_NOTIFICATIONS_URL
prefix {
  path   = "config/db/notifications"
  format = "db_notifications_{{key}}"
}

# Globals for Docker + NewRelic
prefix {
  path = "config/global"
  format = "global_{{key}}"
}

secret {
  path = "secret/global"
}

secret {
  path = "secret/db/notifications"
}

secret {
  path   = "secret/kafka/ssl/non-banking"
}

secret {
    path = "secret/service/rollbar/access_tokens/notifications"
}
