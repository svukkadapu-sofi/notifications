package com.sofi.notifications.config.kafka

import com.sofi.discovery.ServiceDiscovery
import com.sofi.kafka.serialization.AvroSerdeProvider
import com.sofi.notifications.schemas.CreateNotificationRequest
import com.sofi.notifications.schemas.PartyIdKey
import org.apache.kafka.common.serialization.Serde
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile


@Configuration
class KafkaSerdesConfiguration {

    fun schemaRegistryUrl(serviceDiscovery: ServiceDiscovery): String =
            serviceDiscovery.find("schema-registry")?.let { "http://${it.host}:${it.port}" } ?: ""

    @Bean
    @Profile("!test")
    fun avroSerdeProvider(serviceDiscovery: ServiceDiscovery) = AvroSerdeProvider(schemaRegistryUrl(serviceDiscovery))

    @Bean
    fun partyIdKeySerde(serdeProvider: AvroSerdeProvider): Serde<PartyIdKey> = serdeProvider.specificAvroSerde(true)

    @Bean
    fun createNotificationRequestSerde(serdeProvider: AvroSerdeProvider): Serde<CreateNotificationRequest> = serdeProvider.specificAvroSerde(false)
}
