package com.sofi.notifications.config

import com.sofi.discovery.ServiceDiscovery
import com.sofi.discovery.ServiceLocator
import com.sofi.kafka.config.KeyPassword
import com.sofi.kafka.config.KeyStoreFile
import com.sofi.kafka.config.KeyStorePassword
import com.sofi.kafka.config.SslConfig
import com.sofi.kafka.provider.KafkaConsumerProvider
import com.sofi.kafka.provider.KafkaProducerProvider
import com.sofi.kafka.provider.ProducerConfigurations
import io.confluent.kafka.serializers.KafkaAvroDeserializer
import io.confluent.kafka.serializers.KafkaAvroSerializer
import org.apache.avro.specific.SpecificRecord
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.context.annotation.Profile
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory
import org.springframework.kafka.core.*
import com.sofi.kafka.provider.KafkaConfig as SofiKafkaConfig


@Configuration
class SpringKafkaConfig {

    @Value("\${app.name}")
    private val appName: String? = null

    @Value("\${kafka.ssl.enabled}")
    private val sslEnabled: Boolean? = false

    @Value("\${kafka.ssl.clientType}")
    private val clientType: String? = null

    @Value("\${node.ipaddress}")
    private val nodeIp: String? = null

    @Value("\${kafka.truststore.base64}")
    private val trustStoreBase64: String? = null
    @Value("\${kafka.truststore.password}")
    private val trustStorePassword: String? = null
    @Value("\${kafka.keystore.base64}")
    private val keyStoreBase64: String? = null
    @Value("\${kafka.keystore.password}")
    private val keyStorePassword: String? = null
    @Value("\${kafka.key.password}")
    private val keyPassword: String? = null

    @Bean
    fun sslConfig(): SslConfig {
        return SslConfig.create(KeyStoreFile.TRUST_STORE.contentFromBase64(trustStoreBase64),
                KeyStorePassword(trustStorePassword),
                KeyStoreFile.KEY_STORE.contentFromBase64(keyStoreBase64),
                KeyStorePassword(keyStorePassword),
                KeyPassword(keyPassword))
    }

    @Bean
    @Primary
    fun serviceLocator(): ServiceDiscovery {
        // If there is a configured ip available we'll use it. If not we can go to localhost.
        return if (nodeIp != null) {
            ServiceLocator("http://$nodeIp", 8500)
        } else ServiceLocator()
    }

    /**
     * e.g. 'kafka1:9091,kafka2:9092'
     */
    @Bean
    @Profile("!test")
    fun kafkaBootstrapServerProvider(serviceDiscovery: ServiceDiscovery) = serviceDiscovery.findAll("kafka-ssl")
            .joinToString(separator = ",", transform = { "${it.host}:${it.port}" })

    @Bean
    fun kafkaConfig(discovery: ServiceDiscovery,
                    sslConfig: SslConfig): SofiKafkaConfig {
        return SofiKafkaConfig.builder()
                .appName("notifications")
                .enableSsl(sslConfig)
                .discoverUrls(discovery)
                .appName(appName)
                .build()
    }

    @Bean
    fun kafkaProducerProvider(kafkaConfig: SofiKafkaConfig): KafkaProducerProvider {
        return KafkaProducerProvider(ProducerConfigurations.AT_LEAST_ONCE, kafkaConfig)
                .maxBlockDuration(60000)
                .keySerializer(KafkaAvroSerializer::class.java)
                .valueSerializer(KafkaAvroSerializer::class.java)
    }

    @Bean
    fun kafkaConsumerProvider(kafkaConfig: com.sofi.kafka.provider.KafkaConfig): KafkaConsumerProvider {
        return KafkaConsumerProvider(kafkaConfig)
                .defaultOffset(KafkaConsumerProvider.DefaultOffset.EARLIEST)
                .autoCommitWithInterval(100)
                .autoCommit(true)
                .sessionTimeoutInterval(15000)
                .keyDeserializer(KafkaAvroDeserializer::class.java)
                .valueDeserializer(KafkaAvroDeserializer::class.java)
    }

    @Bean
    @Profile("!test")
    fun producerFactory(kafkaProducerProvider: KafkaProducerProvider): ProducerFactory<SpecificRecord, SpecificRecord> {
        return DefaultKafkaProducerFactory(kafkaProducerProvider.buildProperties())
    }

    @Bean
    @Profile("!test")
    fun consumerFactory(kafkaConsumerProvider: KafkaProducerProvider): ConsumerFactory<SpecificRecord, SpecificRecord> {
        return DefaultKafkaConsumerFactory(kafkaConsumerProvider.buildProperties())
    }

    @Bean
    fun kafkaListenerContainerFactory(consumerFactory: ConsumerFactory<SpecificRecord, SpecificRecord>): ConcurrentKafkaListenerContainerFactory<SpecificRecord, SpecificRecord> {
        val factory = ConcurrentKafkaListenerContainerFactory<SpecificRecord, SpecificRecord>()
        factory.consumerFactory = consumerFactory
        return factory
    }

    @Bean
    fun kafkaTemplate(producerFactory: ProducerFactory<SpecificRecord, SpecificRecord>): KafkaTemplate<SpecificRecord, SpecificRecord> {
        return KafkaTemplate(producerFactory)
    }
}
