package com.sofi.notifications.config

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import org.davidmoten.rx.jdbc.Database
import org.davidmoten.rx.jdbc.pool.Pools
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import java.sql.SQLException
import javax.sql.DataSource


@Configuration
@Profile("!test")
class DatabaseConfig {

    @Bean
    fun postgresDataSource(@Value("\${app.db.url}") url: String,
                           @Value("\${app.db.user}") user: String,
                           @Value("\${app.db.max-connections}") maxConnections: Int,
                           @Value("\${app.db.pass}") password: String): DataSource {
        val config = HikariConfig()
        config.jdbcUrl = url
        config.username = user
        config.password = password
        config.maximumPoolSize = maxConnections
        config.poolName = "notificationsDbPool"

        return HikariDataSource(config)
    }

    @Bean
    fun database(dataSource: DataSource): Database {
        try {
            return tryInitDatabase(dataSource)
        } catch (e: SQLException) {
            throw RuntimeException(e)
        }
    }

    @Throws(SQLException::class)
    private fun tryInitDatabase(dataSource: DataSource): Database {
        val nonBlockingPool = Pools.nonBlocking()
                .maxPoolSize(Runtime.getRuntime().availableProcessors() * 5)
                .connectionProvider(dataSource)
                .build()

        return Database.from(nonBlockingPool)
    }
}
