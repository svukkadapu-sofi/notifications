package com.sofi.notifications.config.kafka

import com.sofi.kafka.config.SslConfig
import org.apache.kafka.streams.StreamsConfig
import org.springframework.stereotype.Component
import java.util.*
import javax.inject.Inject

@Component
class StreamsConfigProvider @Inject internal constructor(
        private val kafkaBootstrapServerProvider: String,
        private val sslConfig: SslConfig) {

    fun buildStreamsConfig(appIdSuffix: String): Properties {
        // buildProperties are only ever Strings, so let's clean that for the type system
        val propertyMap = sslConfig.buildProperties().mapValues { it.value.toString() }

        // we're just adding to these, so we convert to Properties then setProperty(), rather than map.putAll(pairs) then convert
        return propertyMap.toProperties().apply {
            // we just set the properties as normal
            setProperty(StreamsConfig.APPLICATION_ID_CONFIG, "NOTIFICATIONS-CENTER-$appIdSuffix")
            setProperty(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaBootstrapServerProvider)
        }
    }
}
