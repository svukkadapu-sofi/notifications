package com.sofi.notifications.config

import com.fasterxml.jackson.databind.ObjectMapper
import com.sofi.bouncer.api.BouncerHandler
import com.sofi.bouncer.api.BouncerHeaders
import com.sofi.security.play.HmacSHA1Signer
import com.sofi.security.play.PlaySessionHandler
import com.sofi.spring.security.bouncer.BouncerAuthenticationProvider
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.actuate.autoconfigure.security.reactive.EndpointRequest
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.http.HttpStatus
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.ReactiveAuthenticationManagerAdapter
import org.springframework.security.authentication.RememberMeAuthenticationToken
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity
import org.springframework.security.config.web.server.ServerHttpSecurity
import org.springframework.security.core.authority.AuthorityUtils
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextImpl
import org.springframework.security.web.server.SecurityWebFilterChain
import org.springframework.security.web.server.authentication.HttpStatusServerEntryPoint
import org.springframework.security.web.server.context.ServerSecurityContextRepository
import org.springframework.stereotype.Component
import org.springframework.web.server.ServerWebExchange
import reactor.core.publisher.Mono
import java.io.IOException
import java.security.InvalidKeyException
import java.security.NoSuchAlgorithmException
import java.util.stream.Stream
import javax.inject.Inject

@Configuration
@EnableWebFluxSecurity
class SecurityConfig @Inject constructor(private val securityContextRepository: ServerSecurityContextRepository,
                                         private val authenticationProvider: AuthenticationProvider) {

    private final val authenticationManager = AuthenticationManager { authentication -> authenticationProvider.authenticate(authentication) }
    val reactiveAuthenticationManagerAdapter = ReactiveAuthenticationManagerAdapter(authenticationManager)

    @Bean
    fun springSecurityFilterChain(http: ServerHttpSecurity): SecurityWebFilterChain {

        //disable default security
        http.httpBasic().disable()
                .formLogin().disable()
                .csrf().disable()
                .logout().disable()

                //add custom bouncer auth
                .authenticationManager(reactiveAuthenticationManagerAdapter)
                .securityContextRepository(securityContextRepository)

                //no security for health endpoint
                .authorizeExchange()
                .matchers(EndpointRequest.to("health"))
                .permitAll()
                .and()

                //no security for legacy endpoints
                .authorizeExchange()
                .pathMatchers("/customers/**")
                .permitAll()
                .and()

                //authorize all other endpoints
                .authorizeExchange()
                .anyExchange()
                .authenticated()
                .and()

                //throw 403 for authentication failures
                .exceptionHandling()
                .authenticationEntryPoint(HttpStatusServerEntryPoint(HttpStatus.FORBIDDEN))

        return http.build()
    }
}

@Component
@Profile("!test")
class BouncerSecurityContextRepository constructor(@Inject private val handler: BouncerHandler) : ServerSecurityContextRepository {
    private val log = LoggerFactory.getLogger(BouncerSecurityContextRepository::class.java)

    override fun save(exchange: ServerWebExchange, context: SecurityContext): Mono<Void> {
        return exchange.session
                .flatMap { session -> session.changeSessionId() }
    }

    override fun load(exchange: ServerWebExchange?): Mono<SecurityContext> {
        val request = exchange?.request

        if (request?.headers?.get(BouncerHeaders.SIGNED) != null) {
            try {
                val session = handler.signedStringToSession(request.headers.getFirst(BouncerHeaders.SIGNED))
                val perms = Stream.concat(session.permissions.stream().map { p -> "ROLE_$p" },
                        Stream.of("ROLE_" + session.role))
                        .toArray<String> { length -> arrayOfNulls(length) }

                val authenticationToken = RememberMeAuthenticationToken(session.uuid, session, AuthorityUtils.createAuthorityList(*perms))
                return Mono.justOrEmpty(SecurityContextImpl(authenticationToken))
            } catch (e: IOException) {
                log.error("Error decoding bouncer header", e)
            }
        }
        return Mono.empty()
    }
}

@Configuration
@Profile("!test")
class BouncerConfig {

    @Bean
    fun bouncerAuthenticationProvider(): AuthenticationProvider {
        return BouncerAuthenticationProvider()
    }

    @Bean
    fun bouncerHandler(mapper: ObjectMapper, interop: PlaySessionHandler): BouncerHandler {
        return BouncerHandler(mapper, interop::decode)
    }

    @Bean
    @Throws(InvalidKeyException::class, NoSuchAlgorithmException::class)
    fun playInterop(@Value("\${auth.secret}") appSecret: String): PlaySessionHandler {
        return PlaySessionHandler(HmacSHA1Signer(appSecret))
    }
}
