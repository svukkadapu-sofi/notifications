package com.sofi.notifications.model

import com.sofi.notifications.db.NotificationEntity
import com.sofi.notifications.schemas.NotificationResult

/**
 * Converts [this] into a Kafka-ready [NotificationResult].
 *
 * TODO: change in concordance with SOFI-62298 (when the NotificationResult impl. ditches joda time for JDK 8 time)
 */
fun NotificationEntity.toNotificationResult() = NotificationResult(
        communicationId(),
        partyId(),
        status(),
        org.joda.time.Instant.ofEpochMilli(eventDate().toEpochMilli()).toDateTime()
)
