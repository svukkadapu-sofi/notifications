package com.sofi.notifications.model

import com.sofi.notifications.db.NotificationEntity
import org.ocpsoft.prettytime.PrettyTime
import org.ocpsoft.prettytime.units.JustNow
import org.ocpsoft.prettytime.units.Millisecond
import java.time.Instant
import java.util.*

data class NotificationDto(
        val id: String?,
        val message: String,
        val nativePath: String?,
        val webUrl: String?,
        val urgent: Boolean = false,
        val read: Boolean = false,
        val time: String = Instant.now().toString()
) {

    companion object {
        @JvmStatic
        fun fromEntity(entity: NotificationEntity): NotificationDto {
            val time = PrettyTime()
            time.removeUnit(JustNow::class.java) //removes "moments ago" for times under a minute
            time.removeUnit(Millisecond::class.java) //rounds milliseconds up to seconds

            return NotificationDto(
                    id = entity.notificationId().toString(),
                    message = entity.message(),
                    time = time.format(Date.from(entity.eventDate())),
                    nativePath = entity.nativePath(),
                    webUrl = entity.webUrl(),
                    urgent = entity.urgent(),
                    read = entity.status() == "READ" || entity.status() == "ACTION_TAKEN"
            )
        }
    }
}
