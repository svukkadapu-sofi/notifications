package com.sofi.notifications.model

data class NotificationPageDto(
        val notifications: List<NotificationDto>,
        var next: String?
) {
    companion object {
        @JvmStatic
        fun fromList(notifications: List<NotificationDto>, count: Int): NotificationPageDto {

            return if (notifications.size <= count) {
                NotificationPageDto(notifications, null)
            } else {
                //take the extra notification out of the list
                notifications.dropLast(1).let {
                    NotificationPageDto(it, toNextUrl(it.last().id!!, count))
                }
            }

        }

        private fun toNextUrl(id: String, count: Int): String {
            return "/notifications?count=$count&after=$id"
        }
    }
}
