package com.sofi.notifications.model

data class CountDto(
        val count: Long
)

