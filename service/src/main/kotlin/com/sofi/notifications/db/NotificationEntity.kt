package com.sofi.notifications.db

import com.sofi.notifications.model.NotificationDto
import org.davidmoten.rx.jdbc.annotations.Column
import java.time.Instant
import java.util.*

interface NotificationEntity {

    @Column
    fun notificationId(): UUID

    @Column
    fun partyId(): Long

    @Column
    fun communicationId():String

    @Column
    fun message():String

    @Column
    fun status(): String

    @Column
    fun nativePath(): String?

    @Column
    fun webUrl(): String?

    @Column("event_dt")
    fun eventDate() : Instant

    @Column
    fun urgent() : Boolean

    @Column
    fun origin(): String
}

fun NotificationEntity.toDto(): NotificationDto = NotificationDto.fromEntity(this)
