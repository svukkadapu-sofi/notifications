package com.sofi.notifications.db

import org.davidmoten.rx.jdbc.Database
import org.slf4j.LoggerFactory
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.time.Instant
import java.util.*
import javax.inject.Inject
import javax.inject.Named

interface NotificationRepository {
    fun createNotification(partyId: Long,
                           communicationId: String,
                           message: String,
                           nativePath: String?,
                           webUrl: String?,
                           eventDt: Instant,
                           urgent: Boolean = false,
                           origin: String): Mono<NotificationEntity>

    fun fetchUnseenNotificationsCount(partyId: Long): Mono<Long>
    fun fetchNotificationsPage(partyId: Long, count: Int, afterNotificationId: UUID?): Flux<NotificationEntity>
    fun markUnseenAsUnread(partyId: Long): Flux<NotificationEntity>
    fun markUnreadAsRead(partyId: Long): Flux<NotificationEntity>
    fun touchNotification(partyId: Long, notificationId: UUID): Mono<NotificationEntity>
}

@Named
open class PostgresNotificationRepository @Inject constructor(private val db: Database) : NotificationRepository {

    override fun createNotification(partyId: Long,
                                    communicationId: String,
                                    message: String,
                                    nativePath: String?,
                                    webUrl: String?,
                                    eventDt: Instant,
                                    urgent: Boolean,
                                    origin: String): Mono<NotificationEntity> {

        log.info("""NotificationRepository.createNotification(
            |partyId = [$partyId], communicationId = [$communicationId], nativePath = [$nativePath],
            |webUrl = [$webUrl], eventDt = [$eventDt], urgent = [$urgent], origin = [$origin])""".trimMargin())

        return Mono.from(db.select("""INSERT INTO notification (party_id,
                                                               |communication_id,
                                                               |message,
                                                               |status,
                                                               |native_path,
                                                               |web_url,
                                                               |event_dt,
                                                               |urgent,
                                                               |origin)
                                             |VALUES  (:partyId,
                                                      |:communicationId,
                                                      |:message,
                                                      |'UNSEEN',
                                                      |:nativePath,
                                                      |:webUrl,
                                                      |:eventDt,
                                                      |:urgent,
                                                      |:origin)
                                             RETURNING *""".trimMargin())
                .parameter("partyId", partyId)
                .parameter("communicationId", communicationId)
                .parameter("message", message)
                .parameter("nativePath", nativePath)
                .parameter("webUrl", webUrl)
                .parameter("eventDt", eventDt)
                .parameter("urgent", urgent)
                .parameter("origin", origin)
                .autoMap(NotificationEntity::class.java))

    }

    override fun fetchUnseenNotificationsCount(partyId: Long): Mono<Long> {
        log.info("NotificationRepository.fetchUnseenNotificationsCount(partyId = [$partyId])")
        return Mono
                .from(db.select("""SELECT count(*) FROM notification WHERE party_id = :partyId and status = 'UNSEEN'""")
                        .parameter("partyId", partyId)
                        .getAs(Long::class.java))
    }

    override fun fetchNotificationsPage(partyId: Long, count: Int, afterNotificationId: UUID?): Flux<NotificationEntity> {
        log.info("NotificationRepository.fetchNotificationsPage(partyId = [$partyId], count = [$count], afterNotificationId = [$afterNotificationId])")

        val notificationId = Optional.ofNullable(afterNotificationId)

        val query = StringBuilder("SELECT * FROM notification WHERE party_id = :partyId")
        notificationId.ifPresent { query.append(" AND event_dt < (SELECT event_dt FROM notification WHERE party_id = :partyId AND notification_id = :notificationId)") }
        query.append(" ORDER BY event_dt DESC LIMIT :count")

        val builder = db.select(query.toString())
                .parameter("partyId", partyId)
                .parameter("count", count)

        notificationId.ifPresent { id -> builder.parameter("notificationId", id) }

        return Flux.from(builder.autoMap(NotificationEntity::class.java))
    }

    override fun markUnseenAsUnread(partyId: Long): Flux<NotificationEntity> {
        log.info("NotificationRepository.markUnseenAsUnread(partyId = [$partyId])")
        return Flux.from(db.select("""UPDATE notification
                                     | SET status = 'UNREAD', updated_dt = now(), updated_by = 'automation@sofi.org'
                                     | WHERE party_id = :partyId  AND status = 'UNSEEN'
                                     | RETURNING *""".trimMargin())
                .parameter("partyId", partyId)
                .autoMap(NotificationEntity::class.java))
    }

    override fun markUnreadAsRead(partyId: Long): Flux<NotificationEntity> {
        return Flux.from(db.select("""UPDATE notification
                                     | SET status = 'READ', updated_dt = now(), updated_by = 'automation@sofi.org'
                                     | WHERE party_id = :partyId  AND status = 'UNREAD'
                                     | RETURNING *""".trimMargin())
                .parameter("partyId", partyId)
                .autoMap(NotificationEntity::class.java))
    }

   override fun touchNotification(partyId: Long, notificationId: UUID): Mono<NotificationEntity> {
        log.info("NotificationRepository.touchNotification(partyId = [$partyId], notificationId = [$notificationId])")
        return Mono.from(db.select("""UPDATE notification
                                         | SET status     = CASE
                                         |   WHEN web_url IS NULL AND native_path IS NULL THEN 'READ'
                                         |   ELSE 'ACTION_TAKEN' END,
                                         |  updated_dt = now(),
                                         |  updated_by = 'automation@sofi.org'
                                         | WHERE notification_id = :notificationId
                                         | AND party_id = :partyId
                                         | RETURNING *""".trimMargin())
                .parameter("notificationId", notificationId)
                .parameter("partyId", partyId)
                .autoMap(NotificationEntity::class.java))
    }

    companion object {
        private val log = LoggerFactory.getLogger(NotificationRepository::class.java)
    }
}

