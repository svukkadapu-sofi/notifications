package com.sofi.notifications.legacy

import com.sofi.notifications.legacy.web.Rules
import org.jdbi.v3.core.Jdbi
import org.jdbi.v3.postgres.PostgresPlugin
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import javax.inject.Inject
import javax.sql.DataSource

@Configuration
class Config {

    @Bean
    fun tableName() = "notifications"

    /**
     * Configuration for a [DataSource] to talk to Postgres.
     */
    @Bean
    fun dataSource(@Value("\${app.db.url}") url: String,
                   @Value("\${app.db.user}") user: String,
                   @Value("\${app.db.pass}") password: String): Jdbi {
        return Jdbi.create(url, user, password).installPlugin(PostgresPlugin())
    }

    @Bean
    @Inject
    fun databaseValidationRules(databaseValidation: Rules.DataValidation) = Rules(databaseValidation)
}
