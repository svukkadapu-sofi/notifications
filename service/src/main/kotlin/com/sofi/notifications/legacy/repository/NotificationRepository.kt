package com.sofi.notifications.legacy.repository

import com.sofi.notifications.legacy.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono


interface NotificationRepository {
    /**
     * @return a resultant page of notifications
     */
    fun getPage(customerId: CustomerId, pageNumber: Int, pageLength: Int): Flux<Notification>

    /**
     * @return a resultant page of notifications
     */
    fun hasNew(customerId: CustomerId): Mono<HasNewStatus>

    /**
     * @return the single requested notification
     */
    fun getOneNotification(forCustomerId: CustomerId, notificationId: NotificationId): Mono<Notification>

    /**
     * Does not rollback the status of a Notification
     * @return if the notification was successfully updated
     */
    fun updateSingleStatus(customerId: CustomerId,
                           notificationId: NotificationId,
                           newStatus: NotificationStatus = NotificationStatus.UNREAD): Mono<NotificationDbUpdateResponse>

    fun markAllAsRead(customerId: CustomerId): Mono<Void>
    fun createNotification(partyId: CustomerId, payload: CreateNotificationPayload): Mono<NotificationId>
}
