package com.sofi.notifications.legacy.web

import com.fasterxml.jackson.databind.ObjectMapper
import com.sofi.notifications.legacy.*
import org.slf4j.LoggerFactory

/**
 * Rules for what is, or is not, valid in the notifications domain
 */
class Rules(private val dataValidator: DataValidation) {

    private val log = LoggerFactory.getLogger(Rules::class.java)

    /**
     * Returns a pairing of
     * a) whether, or not, a customerId is valid
     * b) an error message as to how if invalid
     */
    fun checkValidCustomerRequest(customerId: CustomerId?): Pair<Boolean, String> {
        log.debug("Rules.checkValidCustomerRequest")
        log.debug("customerId = [$customerId]")
        return when (customerId) {
            is CustomerId -> when {
                dataValidator.hasCustomerId(customerId) -> true to ""
                else -> false to "Could not find customer with identifier $customerId"
            }
            else -> false to "Couldn't parse $customerId as customerId"
        }
    }

    fun checkValidNotificationRequest(customerId: CustomerId?, notificationId: NotificationId?): Pair<Boolean, String> {
        log.debug("customerId = $customerId, notificationId = $notificationId")

        val (isValid, errorMessage) = checkValidCustomerRequest(customerId)

        if (!isValid) return isValid to errorMessage

        return when (notificationId) {
            is NotificationId -> when {
                dataValidator.hasNotificationId(customerId as CustomerId, notificationId) -> true to ""
                else -> false to "Could not find notification with id $notificationId for customer with id $customerId"
            }
            else -> false to "Couldn't parse $notificationId as notificationId"
        }
    }

    fun checkValidNotificationStatusBody(body: String): Triple<Boolean, String, NotificationStatus> {
        // used for explaining invalidity of a body
        val failInstance = Triple(false, "Could not read $body for valid update status", NotificationStatus.Invalid)

        log.debug("body = $body")

        // exit early if not json
        if (!body.startsWith("{")) return failInstance

        // if json, get "status" property of said json
        val statusInJson = ObjectMapper().readValue(body, HashMap::class.java)["status"] as String?
        val statusProp = statusInJson ?: ""

        // check valid status string: "READ|UNREAD|NEW"
        return if (statusProp in NotificationStatus.statusesAsStrings) {
            Triple(true, "", NotificationStatus.valueOf(statusProp))
        } else failInstance
    }

    fun checkValidNotificationCreateBody(body: String): Triple<Boolean, String, CreateNotificationPayload> {
        log.debug("Rules.checkValidNotificationCreateBody")
        log.debug("body = $body")
        // JSON only
        if (!body.trim().startsWith("{")) return Triple(false, "not valid JSON body", CreateNotificationPayload.nullInstance)

        val bodyJson = ObjectMapper().readTree(body)
        val messageParsed = bodyJson["message"].asText()

        require(messageParsed.isNotBlank()) { "Message property must not be blank" }

        val constructedPayload = if (bodyJson.has("callToAction") && !bodyJson["callToAction"].isNull) {
            val ctaInJson = bodyJson["callToAction"]
            val textProp = ctaInJson["text"].asText()
            val urlProp = ctaInJson["url"].asText()
            require(textProp.isNotBlank()) { "Text of the callToAction must not be blank" }
            require(urlProp.isNotBlank()) { "Url of the callToAction must not be blank" }
            CreateNotificationPayload(messageParsed, CallToAction(textProp, urlProp))
        } else CreateNotificationPayload(messageParsed, null)

        return Triple(true, "", constructedPayload)
    }

    fun checkValidPagingParameters(pageNumber: Int?, pageLength: Int?): Pair<Boolean, String> {
        log.debug("checkValidPagingParameters: pageNumber = $pageNumber, pageLength = $pageLength")
        if (pageNumber == null || pageLength == null) return false to "couldn't parse 'pageLength' or 'pageNumber' as valid integers"

        if (pageLength < 0) return false to "'pageLength' must be greater than 0"
        if (pageNumber < 0) return false to "'pageNumber' must be greater than 0"

        return true to ""
    }

    interface DataValidation {
        fun hasCustomerId(id: CustomerId): Boolean
        fun hasNotificationId(forCustomerId: CustomerId, notificationId: NotificationId): Boolean
    }
}
