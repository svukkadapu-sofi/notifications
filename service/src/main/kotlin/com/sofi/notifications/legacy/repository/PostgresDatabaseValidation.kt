package com.sofi.notifications.legacy.repository

import com.sofi.notifications.legacy.CustomerId
import com.sofi.notifications.legacy.NotificationId
import com.sofi.notifications.legacy.web.Rules
import org.jdbi.v3.core.Jdbi
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import java.util.*

@Component
class PostgresDatabaseValidation(
        private val db: Jdbi,
        private val tableName: String
) : Rules.DataValidation {

    private val log = LoggerFactory.getLogger(PostgresDatabaseValidation::class.java)


    override fun hasCustomerId(id: CustomerId): Boolean = db.withHandle<Optional<CustomerId>, Exception> {
        log.debug("Checking database for customerId $id")
        it.createQuery("SELECT party_id from $tableName WHERE party_id = $id limit 1")
                .mapTo(CustomerId::class.java)
                .findFirst()
    }.isPresent

    override fun hasNotificationId(forCustomerId: CustomerId,
                                   notificationId: NotificationId) = db.withHandle<Optional<Int>, Exception> {
        log.debug("Checking database for notification $notificationId under customer $forCustomerId")
        it.createQuery("""select 1 from $tableName
                |where extract_notification_ids(notifications) @> ARRAY[:notification_id]::text[]
                """.trimMargin())
                .bind("notification_id", notificationId.toString())
                .mapTo(Int::class.java)
                .findFirst()
    }.isPresent
}
