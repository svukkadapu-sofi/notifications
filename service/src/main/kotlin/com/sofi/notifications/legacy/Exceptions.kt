package com.sofi.notifications.legacy

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

/* For reporting errors to the client */

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Customer not found")
class CustomerNotFoundException(message: String) : RuntimeException(message)

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Could not parse as customer id")
class InvalidCustomerRequestException(customerIdAttempt: String) : RuntimeException("Could not parse $customerIdAttempt as a customer id")

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Paging parameters are invalid")
class BadNotificationPageParametersException(message: String) : RuntimeException(message)

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Notification not found")
class NotificationNotFoundException(message: String) : RuntimeException(message)

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Bad notification body")
class NotificationBodyNotParseableException(message: String) : RuntimeException(message)
