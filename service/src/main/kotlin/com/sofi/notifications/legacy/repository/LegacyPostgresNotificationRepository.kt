package com.sofi.notifications.legacy.repository

import com.sofi.notifications.legacy.*
import org.jdbi.v3.core.Jdbi
import org.jdbi.v3.core.statement.StatementContext
import org.postgresql.util.PSQLException
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.sql.ResultSet
import javax.inject.Inject

@Repository
class LegacyPostgresNotificationRepository(
        @Inject private val db: Jdbi,
        private val tableName: String = "notifications"
) : NotificationRepository {

    private val log = LoggerFactory.getLogger(LegacyPostgresNotificationRepository::class.java)

    /**
     * Converts a [ResultSet] to a [Notification]
     */
    @Suppress("context")
    private fun notificationFromJson(resultSet: ResultSet, context: StatementContext): Notification {
        log.debug("LegacyPostgresNotificationRepository.notificationFromJson")
        log.debug("resultSet = $resultSet")
        return fromJson(resultSet.getString("n"), Notification::class.java)
    }

    /**
     * Get a page of notifications: some subsection of all notifications this user has ever received.
     * Starting at [pageNumber] 0 to return up to [pageLength] notifications
     */
    override fun getPage(customerId: CustomerId, pageNumber: Int, pageLength: Int): Flux<Notification> {
        log.debug("getPage: customerId = [${customerId}], pageNumber = [${pageNumber}], pageLength = [${pageLength}]")

        val start = pageNumber * pageLength
        val end = start + pageLength

        val withHandle = db.withHandle<Iterable<Notification>, PSQLException> {

            it.createQuery("""select n from (
                select n, nr+first_index-1 total_index from (
                select ni.last_index, ni.id,  last_index-count+1 first_index, notifications, created from (
                    select notifications_id id, party_id, count, created, sum(count) over (order by created desc) last_index, notifications
                    from notifications where party_id = :party_id::bigint
                    order by created desc) ni) f
                    left join lateral unnest(notifications) with ordinality as t(n, nr) on true
                where last_index >= :begin::int
                and   first_index <= :end::int) containing_set
                where total_index between :begin::int and :end::int;
    """.trimIndent())
                    .bind("party_id", customerId)
                    .bind("begin", start)
                    .bind("end", end)
                    .map(::notificationFromJson)
                    .list()
        }
        return Flux.fromIterable(withHandle)
    }

    /**
     * "PartyId": Long
     * "has-new": Boolean
     */
    override fun hasNew(customerId: CustomerId): Mono<HasNewStatus> {
        log.debug("hasNew: customerId = [${customerId}]")
        return Mono.just(db.withHandle<HasNewStatus, PSQLException> {

            log.debug("Querying database for 'hasNew'")

            it.createQuery("""select count(1) bool
                    from notifications
                    where party_id = :party_id::bigint
                    and extract_notification_status(notifications) @> ARRAY['NEW']
                    limit 1
                """).bind("party_id", customerId)
                    .map { rs, _ -> HasNewStatus(customerId, rs.getInt("bool") == 1) }
                    .first()
        })
    }

    override fun getOneNotification(forCustomerId: CustomerId, notificationId: NotificationId): Mono<Notification> {
        log.debug("getOneNotification: forCustomerId = $forCustomerId, notificationId = $notificationId")
        return Mono.just(
                db.withHandle<Notification, PSQLException> {
                    it.createQuery("""
                    select notifications[array_position(extract_notification_ids(notifications), :notification_id::text)] n from $tableName
                    where extract_notification_ids(notifications) @> ARRAY[:notification_id]::text[];
                """.trimIndent())
                            .bind("notification_id", notificationId.toString())
                            .map(::notificationFromJson)
                            .first()
                }
        )
    }

    override fun createNotification(partyId: CustomerId, payload: CreateNotificationPayload): Mono<NotificationId> {
        log.info("createNotification for customerId $partyId, with payload $payload")

        return Mono.just(
                db.withHandle<NotificationId, PSQLException> {
                    it.createQuery("""
            select insert_notification(
                :message,
                :party_id::bigint,
                :title::text,
                :action_text::text,
                :action_url::text
            );
            """.trimIndent())
                            .bind("party_id", partyId)
                            .bind("message", payload.message)
                            .bind("title", "Notification Title")
                            .bind("action_text", payload.callToAction?.text ?: "")
                            .bind("action_url", payload.callToAction?.url ?: "")
                            .mapTo(NotificationId::class.java)
                            .findOnly()
                })
    }

    override fun updateSingleStatus(customerId: CustomerId, notificationId: NotificationId, newStatus: NotificationStatus): Mono<NotificationDbUpdateResponse> {
        log.info("updateSingleStatus: customerId = $customerId, notificationId = $notificationId, newStatus = $newStatus")
        return Mono.just(
                db.withHandle<NotificationDbUpdateResponse, PSQLException> {
                    it.createQuery("select update_notification_status(:notification_id, :new_status) s")
                            .bind("notification_id", notificationId.toString())
                            .bind("new_status", newStatus)
                            .map { rs, _ ->
                                NotificationDbUpdateResponse(notificationId,
                                        newStatus.name == rs.getString("s"),
                                        newStatus)
                            }
                            .first()
                }
        )
    }

    override fun markAllAsRead(customerId: CustomerId): Mono<Void> {
        log.info("markAllAsRead for customerId = $customerId")

        return Mono.just(db.useHandle<Exception> {
            it.createUpdate("""
                update $tableName set notifications = mark_all_as_read(notifications)
                where  party_id = :party_id::bigint
                and (extract_notification_status(notifications) @> ARRAY['NEW'] or extract_notification_status(notifications) @> ARRAY['UNREAD']);
            """.trimIndent())
                    .bind("party_id", customerId)
                    .execute()
        }).then()

    }
}
