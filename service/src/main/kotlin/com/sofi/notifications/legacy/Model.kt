package com.sofi.notifications.legacy

import java.sql.Timestamp

typealias CustomerId = Int

typealias NotificationId = java.util.UUID

data class Notification(
        val id: NotificationId,
        val partyId: CustomerId,
        val callToAction: CallToAction,
        val title: String,
        val message: String,
        var status: NotificationStatus = NotificationStatus.NEW,
        val created: Timestamp
)

data class CallToAction(
        val text: String,
        val url: String
)

data class CreateNotificationPayload(
        val message: String,
        val callToAction: CallToAction?
) {
    companion object {
        val nullInstance = CreateNotificationPayload("", null)
    }
}

data class HasNewStatus(val partyId: CustomerId, val hasNew: Boolean)

data class NotificationDbUpdateResponse(val id: NotificationId, val success: Boolean, val newStatus: NotificationStatus)

enum class NotificationStatus {
    NEW,
    UNREAD,
    READ,
    Invalid // strictly for communicating error within the service
    ;

    companion object {
        private val allStatuses = NotificationStatus.values()

        /**
         * Usage:
         * @code NotificationStatus[1] // NotificationStatus.Unread
         */
        operator fun get(ordinal: Int): NotificationStatus {
            require(ordinal in 0..(allStatuses.size - 1)) { "Invalid NotificationStatus index: $ordinal" }
            return NotificationStatus.values()[ordinal]
        }

        // don't take the last (null-case) instance
        val statusesAsStrings = values().take(allStatuses.size - 1).map { it.toString() }
    }
}
