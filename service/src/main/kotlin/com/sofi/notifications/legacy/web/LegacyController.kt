package com.sofi.notifications.legacy.web

import com.sofi.notifications.legacy.*
import com.sofi.notifications.legacy.repository.NotificationRepository
import org.slf4j.LoggerFactory
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.util.*
import javax.inject.Inject

/**
 * Represents the original, Sparkjava implementation.
 */
@RestController
@RequestMapping("/customers/{customerId}", produces = [MediaType.APPLICATION_JSON_UTF8_VALUE])
class LegacyController(
        @Inject private val rules: Rules,
        @Inject private val repository: NotificationRepository) {

    private val log = LoggerFactory.getLogger(LegacyController::class.java)

    @GetMapping
    fun getPageOfNotifications(@PathVariable customerId: String,
                               @RequestParam pageNumber: Int,
                               @RequestParam pageLength: Int): Flux<Notification> {

        log.debug("LegacyController.getPageOfNotifications")
        log.debug("customerId = [$customerId], pageNumber = [$pageNumber], pageLength = [$pageLength]")
        val customerIdAsInt = customerId.toIntOrNull() ?: throw InvalidCustomerRequestException(customerId)

        val (isValid, errorMessage) = rules.checkValidCustomerRequest(customerIdAsInt)
        if (!isValid) throw CustomerNotFoundException(errorMessage)

        val (isValidPageRequest, pagingErrorMessage) = rules.checkValidPagingParameters(pageNumber, pageLength)
        if (!isValidPageRequest) throw BadNotificationPageParametersException((pagingErrorMessage))

        return repository.getPage(customerIdAsInt, pageNumber, pageLength)
    }

    /**
     * Return a JSON object of
     * [String] "partyId" and [Boolean] "hasNew"
     */
    @GetMapping("/has-new")
    fun doesCustomerHaveNewNotifications(@PathVariable customerId: String): Mono<HasNewStatus> {

        val idAsInt = customerId.toIntOrNull()
        val (isValid, errorMessage) = rules.checkValidCustomerRequest(idAsInt)

        return if (isValid) repository.hasNew(idAsInt!!)
        else throw CustomerNotFoundException(errorMessage)
    }

    /**
     * Returns a single notification as JSON
     *
     * id: [com.sofi.notifications.NotificationId],
     * partyId: CustomerId,
     * callToAction: CallToAction,
     * title: String,
     * message: String,
     * status: NotificationStatus = NotificationStatus.NEW,
     * created: Timestamp
     */
    @GetMapping("/notifications/{notificationId}")
    fun getSpecificNotification(@PathVariable customerId: String, @PathVariable notificationId: String): Mono<Notification> {

        log.debug("LegacyController.getSpecificNotification")
        log.debug("customerId = [$customerId], notificationId = [$notificationId]")

        val customerIdAsInt = customerId.toIntOrNull() ?: throw InvalidCustomerRequestException(customerId)
        val typedNotificationId = notificationId.toUuidOrNull()

        val (isValid, errorMessage) = rules.checkValidNotificationRequest(customerIdAsInt, typedNotificationId)

        return when {
            isValid -> repository.getOneNotification(customerIdAsInt, typedNotificationId!!)
            else -> {
                log.warn(errorMessage)
                throw NotificationNotFoundException(errorMessage)
            }
        }
    }

    /**
     * Create, and return, a new [Notification]'s [NotificationId]
     */
    @PostMapping
    fun createNotification(@PathVariable customerId: String, @RequestBody requestBody: String): Mono<String> {

        log.debug("Parsing $customerId as int")

        val customerIdAsInt = customerId.toIntOrNull() ?: throw InvalidCustomerRequestException(customerId)

        log.debug("Passed validation as $customerIdAsInt")

        val (validRequestBody, bodyErrorMessage, createNotificationPayload) = rules.checkValidNotificationCreateBody(requestBody)
        if (!validRequestBody) {
            log.debug("Parsing notification create request failure")
            log.warn(bodyErrorMessage)
            throw NotificationBodyNotParseableException(bodyErrorMessage)
        }

        return repository.createNotification(customerIdAsInt, createNotificationPayload).map(Any::toJson)
    }

    /**
     * Marks all notifications for a given [customerId] as [com.sofi.notifications.NotificationStatus.READ]
     * or throws [CustomerNotFoundException]
     */
    @PutMapping
    fun markAllNotificationsAsRead(@PathVariable customerId: String): Mono<String> {

        log.debug("Parsing $customerId as int")

        val customerIdAsInt = customerId.toIntOrNull()

        val (isValid, errorMessage) = rules.checkValidCustomerRequest(customerIdAsInt)

        return when {
            isValid -> repository.markAllAsRead(customerIdAsInt!!).then(respondWithSuccessJson())
            else -> {
                log.warn(errorMessage)
                throw CustomerNotFoundException(errorMessage)
            }
        }
    }

    /**
     * Decrements the status of a Notification from [NotificationStatus.NEW] to [NotificationStatus.UNREAD]
     * or [NotificationStatus.UNREAD] to [NotificationStatus.READ].
     *
     * All other transitions are invalid and the response well reflect that in a soft-error message
     */
    @PutMapping("/notifications/{notificationId}")
    fun markOneNotificationAsRead(@PathVariable customerId: String,
                                  @PathVariable notificationId: String,
                                  @RequestBody body: String): Mono<NotificationDbUpdateResponse> {

        val customerIdAsInt = customerId.toIntOrNull()
        val notificationIdTyped = notificationId.toUuidOrNull()

        val (hasValidRequestParams, errorMessage) = rules.checkValidNotificationRequest(customerIdAsInt, notificationIdTyped)
        if (!hasValidRequestParams) log.warn(errorMessage)

        val (hasValidRequestBody, bodyErrorMessage, status) = rules.checkValidNotificationStatusBody(body)
        if (!hasValidRequestBody) log.warn(bodyErrorMessage)

        when {
            customerIdAsInt == null -> {
                throw CustomerNotFoundException(errorMessage)
            }
            !hasValidRequestParams -> {
                throw NotificationNotFoundException(errorMessage)
            }
            !hasValidRequestBody -> {
                throw NotificationBodyNotParseableException(bodyErrorMessage)
            }
            else -> return repository.updateSingleStatus(
                    customerId = customerIdAsInt,
                    notificationId = notificationIdTyped!!,
                    newStatus = status
            )
        }
    }

    /**
     * A simple { "succeeded": true } JSON response, so that the caller sees something alongside the HTTP 200
     */
    private fun respondWithSuccessJson(): Mono<String> = Mono.just(mapOf("succeeded" to true).toJson())

    /**
     * For parsing [NotificationId]
     */
    private fun String.toUuidOrNull(): UUID? = try {
        UUID.fromString(this)
    } catch (e: Exception) {
        null
    }
}
