package com.sofi.notifications.legacy

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule


private val registeredMapper = ObjectMapper().registerKotlinModule()

fun Any.toJson(): String {
    return registeredMapper.writeValueAsString(this)
}

fun <T : Any> fromJson(json: String, t: Class<T>): T = registeredMapper.readValue(json, t)
