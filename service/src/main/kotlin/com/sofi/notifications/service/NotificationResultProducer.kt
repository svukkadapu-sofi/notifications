package com.sofi.notifications.service

import com.newrelic.api.agent.Trace
import com.sofi.notifications.db.NotificationEntity
import com.sofi.notifications.model.toNotificationResult
import com.sofi.notifications.schemas.PartyIdKey
import com.sofi.notifications.topics.NotificationCenterTopics
import org.apache.avro.specific.SpecificRecord
import org.slf4j.LoggerFactory
import org.springframework.kafka.core.KafkaTemplate
import javax.inject.Inject
import javax.inject.Named

@Named
class NotificationResultProducer(@Inject private val kafkaTemplate: KafkaTemplate<SpecificRecord, SpecificRecord>) {

    private val topic = NotificationCenterTopics.NotificationResult!!

    @Trace
    fun sendNotificationResult(notification: NotificationEntity) {
        log.debug("NotificationResultProducer.sendNotificationResult(notification = [$notification])")
        kafkaTemplate.send(topic.topicName, PartyIdKey(notification.partyId()), notification.toNotificationResult())
        log.debug("Sent notification async")
    }

    companion object {
        private val log = LoggerFactory.getLogger(NotificationResultProducer::class.java)
    }
}
