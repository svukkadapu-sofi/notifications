package com.sofi.notifications.service

import com.newrelic.api.agent.Trace
import java.lang.Math.toIntExact

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

import com.sofi.avro.schemas.AppBadgeCount
import com.sofi.avro.schemas.PartyId
import com.sofi.firebase.schemas.Schemas
import com.sofi.firebase.topics.FirebaseTopics
import com.sofi.notifications.db.NotificationRepository
import org.apache.avro.specific.SpecificRecord
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.kafka.core.KafkaTemplate

import javax.inject.Inject

@Service
class BadgeCountProducer @Inject
constructor(private val template: KafkaTemplate<SpecificRecord, SpecificRecord>,
            private val repository: NotificationRepository) {

    fun updateBadgeCount(partyId: Long) {
        repository.fetchUnseenNotificationsCount(partyId)
                  .subscribe({ count -> produceBadgeCountUpdateEvent(partyId, count) },
                             { e -> logBadgeCountFail(partyId, e) })
    }

    @Trace
    private fun produceBadgeCountUpdateEvent(partyId: Long, count: Long) {
        val partyIdKey = PartyId()
            partyIdKey.partyId = partyId

        val appBadgeCount = AppBadgeCount()
            appBadgeCount.partyId = partyId
            appBadgeCount.badgeCount = toIntExact(count)

        template.send(FirebaseTopics.`AppBadgeCount$topicName`, partyIdKey, appBadgeCount)

        log.info("producing to TOPIC {}: KEY {}, VALUE {}", Schemas.AppBadgeCount, partyIdKey, appBadgeCount)
    }

    companion object {

        private val log = LoggerFactory.getLogger(BadgeCountProducer::class.java)

        private fun logBadgeCountFail(partyId: Long, e: Throwable) {
            log.warn("there was a problem handling the badge count for partyId {}", partyId, e)
        }
    }
}

