package com.sofi.notifications.service

import com.sofi.notifications.db.NotificationEntity
import com.sofi.notifications.db.NotificationRepository
import com.sofi.notifications.model.CountDto
import com.sofi.notifications.model.NotificationDto
import org.slf4j.LoggerFactory
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.time.Duration
import java.time.Instant
import java.util.*
import javax.inject.Named

@Named
class NotificationService constructor(
        private val repository: NotificationRepository,
        private val notificationResult: NotificationResultProducer,
        private val badgeCountProducer: BadgeCountProducer) {

    private val log = LoggerFactory.getLogger(NotificationService::class.java)

    fun fetchNotificationsPage(partyId: Long, count: Int, after: UUID?): Flux<NotificationEntity> {
        log.info("fetchNotificationsPage partyId = [$partyId], count = [$count], after = [$after]")

        var fetch = repository.fetchNotificationsPage(partyId, count, after)
        if (after == null) fetch = fetch.doOnTerminate {
            val lastMarkedAsUnread = markUnseenAsUnread(partyId)
            log.info("Marked last notification $lastMarkedAsUnread")
        }
        return fetch
    }

    fun fetchUnseenNotificationsCount(partyId: Long): Mono<CountDto> {
        log.debug("fetching unseen notifications count for partyId = [$partyId]")

        return repository.fetchUnseenNotificationsCount(partyId).map { CountDto(it) }
    }

    fun createNotification(partyId: Long,
                           communicationId: String,
                           message: String,
                           nativePath: String?,
                           webUrl: String?,
                           eventDt: Instant,
                           urgent: Boolean = false,
                           origin: String): Mono<NotificationDto> {

        log.info("""createNotification partyId = [$partyId], communicationId = [$communicationId],
            |nativePath = [$nativePath], webUrl = [$webUrl],
            |eventDate = [$eventDt], urgent = [$urgent], origin = [$origin]""".trimMargin())

        return repository.createNotification(partyId, communicationId, message, nativePath, webUrl, eventDt, urgent, origin)
                .doOnNext { n -> eventToKafka(n) }
                .doOnSuccess { updateBadgeCount(partyId) }
                .map { NotificationDto.fromEntity(it) }
    }

    fun markAllNotificationsAsRead(partyId: Long): Mono<Void> {
        log.info("markAllNotificationsAsRead partyId = [$partyId]")

        return repository.markUnreadAsRead(partyId)
                .doOnNext { n -> eventToKafka(n) }
                .then()
    }

    fun touchNotification(partyId: Long, notificationId: UUID): Mono<Void> {
        log.info("touchNotification partyId = [$partyId], notificationId = [$notificationId]")

        return repository.touchNotification(partyId, notificationId)
                .doOnNext { n -> eventToKafka(n) }
                .then()
    }

    private fun markUnseenAsUnread(partyId: Long): NotificationEntity? {
        log.info("markUnseenAsUnread partyId = [$partyId]")

        return repository.markUnseenAsUnread(partyId)
                .doOnNext { notification -> eventToKafka(notification) }
                .doOnComplete { updateBadgeCount(partyId) }
                .blockLast(Duration.ofSeconds(5))

    }

    private fun eventToKafka(notification: NotificationEntity) {
        log.info("notification to be sent as event = [$notification]")
        notificationResult.sendNotificationResult(notification)
    }

    private fun updateBadgeCount(partyId: Long) {
        badgeCountProducer.updateBadgeCount(partyId)
    }
}
