package com.sofi.notifications.service

import com.sofi.notifications.config.SpringKafkaConfig
import com.sofi.notifications.config.kafka.StreamsConfigProvider
import com.sofi.notifications.schemas.CreateNotificationRequest
import com.sofi.notifications.schemas.PartyIdKey
import com.sofi.notifications.topics.NotificationCenterTopics.`CreateNotificationRequest$topicName`
import org.apache.kafka.common.serialization.Serde
import org.apache.kafka.streams.KafkaStreams
import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.Topology
import org.apache.kafka.streams.kstream.Consumed
import org.apache.kafka.streams.kstream.KStream
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.time.Instant
import javax.annotation.PostConstruct
import javax.inject.Inject

@Service
class CreateNotificationRequestConsumer @Inject constructor(
        val notificationService: NotificationService,
        val streamsConfigProvider: StreamsConfigProvider,
        val keySerde: Serde<PartyIdKey>,
        val createRequestSerde: Serde<CreateNotificationRequest>
) {

    /**
     * Creates a [KafkaStreams] that runs on a background thread, which will terminate when the application stops
     * @see Runtime.addShutdownHook
     * @see KafkaStreams
     */
    @PostConstruct
    fun startNotificationCreationStream() {
        val config = streamsConfigProvider.buildStreamsConfig(APP_ID_SUFFIX)
        val streams = KafkaStreams(buildTopology(), config)
        streams.setUncaughtExceptionHandler(::exceptionHandler)
        try {
            streams.start()
            log.info("Kafka Streams {} started", APP_ID_SUFFIX)
        } catch (e: Exception) {
            log.error("Kafka Streams Exception:", e)
        } finally {
            Runtime.getRuntime().addShutdownHook(Thread { streams.close() })
        }
    }

    /**
     * Constructs a [KStream] to manage our consumer API:
     * whenever a [CreateNotificationRequest] is read, we'll write to our [notificationService]
     *
     * @see KStream<PartyIdKey, CreateNotificationRequest>
     */
    fun buildTopology(): Topology {
        val builder = StreamsBuilder()

        val stream = builder.stream(`CreateNotificationRequest$topicName`, Consumed.with(keySerde, createRequestSerde))

        // we make a notification and log the result: success or failure
        stream.foreach { id, notificationDescription ->
            log.info("Invoked KStream.foreach(id, notificationRequest), communicationId = [${notificationDescription.communicationId}], origin = [${notificationDescription.meta.origin}]")
            val createNotification = notificationService.createNotification(
                    partyId = id.customerId,
                    communicationId = notificationDescription.communicationId,
                    message = notificationDescription.body,
                    nativePath = notificationDescription.nativePath,
                    webUrl = notificationDescription.webUrl,
                    eventDt = notificationDescription.jdkEventTime,
                    origin = notificationDescription.meta.origin,
                    urgent = notificationDescription.meta.urgency
            )

            createNotification.subscribe(
                    { log.debug("Successfully created notification from Kafka") },
                    { log.warn("Failed to create notification from Kafka") },
                    { log.info("Completed attempt to write notification to the database") } // "finally"
            )
        }

        return builder.build()
    }

    companion object {
        private val log = LoggerFactory.getLogger(SpringKafkaConfig::class.java)
        const val APP_ID_SUFFIX = "NOTIFICATION-CREATION"

        /**
         * Log the [throwable] that occurred on [thread]
         */
        fun exceptionHandler(thread: Thread, throwable: Throwable) {
            log.error("Uncaught Kafka-Stream Exception on thread {}", thread.name, throwable)
        }
    }

}

/**
 * Produces an [Instant] from the org.joda.time that is part of the Avro-parsed [com.sofi.notifications.schemas.CreateNotificationRequest].
 * Once SOFI-62298 is complete, this can be omitted for a direct reference
 */
private val CreateNotificationRequest.jdkEventTime: Instant
    get() {
        return Instant.ofEpochMilli(this.meta.eventTime.millis)
    }
