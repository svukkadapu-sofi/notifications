package com.sofi.notifications

import com.sofitracing.api.spring.newrelic.EnableNewRelicAsyncLinking
import com.sofitracing.api.spring.webflux.EnableWebFluxTracing
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.kafka.annotation.EnableKafka

@EnableKafka
@EnableWebFluxTracing
@EnableNewRelicAsyncLinking
@SpringBootApplication
class App

// Main controllers are web/Legacy and web/Notifications
fun main(args: Array<String>) {
    runApplication<App>(*args)
}
