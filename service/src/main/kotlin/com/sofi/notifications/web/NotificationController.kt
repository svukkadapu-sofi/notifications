package com.sofi.notifications.web

import com.sofi.bouncer.api.BouncerSession
import com.sofi.notifications.model.CountDto
import com.sofi.notifications.model.NotificationDto.Companion.fromEntity
import com.sofi.notifications.model.NotificationPageDto
import com.sofi.notifications.service.NotificationService
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono
import java.util.*
import javax.inject.Inject

/**
 * Bouncer will interact with this, so we already have the customer id.
 * Passing it along would be redundant.
 */
@RestController()
class NotificationController @Inject constructor(private val service: NotificationService) {

    @GetMapping("/notifications")
    fun notificationsPage(@RequestParam(required = false) after: UUID?,
                          @RequestParam(required = false, defaultValue = "10") count: Int,
                          @AuthenticationPrincipal session: BouncerSession): Mono<NotificationPageDto> {

        //fetch an extra record so we know if there is a next page
        val fetchRecordCount = count + 1

        return service.fetchNotificationsPage(session.partyId, fetchRecordCount, after)
                .map(::fromEntity)
                .collectList()
                .map { NotificationPageDto.fromList(it, count) }
    }

    @GetMapping("/notifications/count/unseen")
    fun notificationsPage(@AuthenticationPrincipal session: BouncerSession): Mono<CountDto> {
        return service.fetchUnseenNotificationsCount(session.partyId)
    }

    @PutMapping("/notifications/markallasread")
    fun markAllNotificationsAsRead(@AuthenticationPrincipal session: BouncerSession): Mono<Void> {
        return service.markAllNotificationsAsRead(session.partyId)
    }

    @PutMapping("/notifications/{notificationId}/touch")
    fun touchNotification(@PathVariable notificationId: UUID, @AuthenticationPrincipal session: BouncerSession): Mono<Void> {
        return service.touchNotification(session.partyId, notificationId)
    }
}
