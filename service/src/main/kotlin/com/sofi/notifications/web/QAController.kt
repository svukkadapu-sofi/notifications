package com.sofi.notifications.web

import com.sofi.bouncer.api.BouncerSession
import com.sofi.notifications.db.NotificationEntity
import com.sofi.notifications.model.NotificationDto
import com.sofi.notifications.service.NotificationService
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Profile
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono
import java.time.Instant
import java.time.format.DateTimeFormatter
import java.util.*
import javax.inject.Inject

/**
 * This controller is intended for QA only and requires the spring profile "qa" to be active. This should happen
 * automatically for docker environments that are not prod. To add the profile to your dev environment,
 * add the environment variable SPRING_PROFILES_ACTIVE and set it to "default,qa"
 *
 * Bouncer will interact with this, so we already have the customer id.
 * Passing it along would be redundant.
 */
@Profile("qa")
@RestController()
@RequestMapping("/qa")
class QAController @Inject constructor(private val service: NotificationService) {

    private val QA_ORIGIN = "qa"
    private val log = LoggerFactory.getLogger(this::class.java)

    @PostMapping(path=["/notifications","/notifications/{communicationId}"])
    fun notificationsPage(@PathVariable(name="communicationId", required = false) passedCommunicationId: String?,
                          notification: NotificationDto,
                          @AuthenticationPrincipal session: BouncerSession): Mono<NotificationDto> {
        val partyId = session.partyId
        val communicationId = passedCommunicationId ?: "QA-${UUID.randomUUID()}"

        log.info("QA partyId {} creating notification {} using communicationId {}", partyId,  notification, communicationId)
        return service.createNotification(session.partyId,
                                          communicationId,
                                          notification.message,
                                          notification.nativePath,
                                          notification.webUrl,
                                          DateTimeFormatter.ISO_INSTANT.parse(notification.time, Instant::from),
                                          notification.urgent,
                                          QA_ORIGIN)
    }
}

