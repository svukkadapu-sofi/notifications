package com.sofi.notifications

import com.sofi.firebase.topics.FirebaseTopics
import com.sofi.notifications.topics.NotificationCenterTopics
import org.apache.avro.specific.SpecificRecord
import org.apache.kafka.clients.consumer.Consumer
import org.apache.kafka.clients.consumer.ConsumerRecords
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.springframework.kafka.annotation.EnableKafka
import org.springframework.kafka.core.ConsumerFactory
import org.springframework.kafka.test.EmbeddedKafkaBroker
import org.springframework.kafka.test.context.EmbeddedKafka
import org.springframework.kafka.test.utils.KafkaTestUtils
import javax.inject.Inject

@EnableKafka
@EmbeddedKafka(topics = [FirebaseTopics.`AppBadgeCount$topicName`, NotificationCenterTopics.`NotificationResult$topicName`])
internal class BaseKafkaTest : BaseIntegrationTest() {

    @Inject
    lateinit var embeddedKafka: EmbeddedKafkaBroker

    @Inject
    lateinit var consumerFactory: ConsumerFactory<SpecificRecord, SpecificRecord>

    lateinit var consumer: Consumer<SpecificRecord, SpecificRecord>

    /**
     * This will return all the records that were sent to the kafka container.
     * It looks a little odd, but the "do-while" with the timeout is to make sure we wait long enough for the records to be populated
     */
    fun <T> getSentRecords(chosenTopic: String): List<T> {
        Thread.sleep(100)
        val recs = ArrayList<T>()
        do {
            val records = KafkaTestUtils.getRecords(consumer, 1000) as ConsumerRecords<SpecificRecord, SpecificRecord>
            if (!records.isEmpty) {
                records.records(chosenTopic).forEach {
                    recs.add(it as T)
                }
            }
        } while (!records.isEmpty)
        return recs
    }

    @BeforeEach
    fun createRecordListener() {
        consumer = consumerFactory.createConsumer()
        consumer.seekToEnd(consumer.assignment())
        embeddedKafka.consumeFromEmbeddedTopics(consumer, *embeddedKafka.topics.toTypedArray())
    }

    @AfterEach
    fun stopContainer() {
        consumer.commitSync()
        consumer.unsubscribe()
    }
}
