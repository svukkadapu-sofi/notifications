package com.sofi.notifications

import com.opentable.db.postgres.embedded.DatabasePreparer
import com.opentable.db.postgres.embedded.FlywayPreparer
import com.opentable.db.postgres.junit.PreparedDbRule
import org.junit.jupiter.api.extension.AfterAllCallback
import org.junit.jupiter.api.extension.BeforeAllCallback
import org.junit.jupiter.api.extension.ExtensionContext

class PreparedDbExtension
constructor(preparer: DatabasePreparer) : PreparedDbRule(preparer), BeforeAllCallback, AfterAllCallback {

    override fun beforeAll(context: ExtensionContext) {
        try {
            before()
        } catch (throwable: Throwable) {
            throw RuntimeException(throwable)
        }
    }

    @Throws(Throwable::class)
    override fun before() = super.before()

    override fun afterAll(context: ExtensionContext) = after()

    companion object {
        fun preparedDatabase(): PreparedDbExtension {
            return PreparedDbExtension(FlywayPreparer.forClasspathLocation("db.premigration", "db.migration", "db.aftermigration"))
        }
    }
}
