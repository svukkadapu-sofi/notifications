package com.sofi.notifications

import com.sofi.kafka.serialization.AvroSerdeProvider

import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient

class MockAvroSerdeProvider(schemaRegistryUrls: String, registryClient: SchemaRegistryClient) : AvroSerdeProvider(schemaRegistryUrls) {

    init {
        schemaRegistryClient(schemaRegistryUrls, registryClient)
    }
}
