package com.sofi.notifications

import com.sofi.kafka.serialization.AvroSerdeProvider
import io.confluent.kafka.schemaregistry.client.MockSchemaRegistryClient
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient
import io.confluent.kafka.serializers.KafkaAvroDeserializer
import io.confluent.kafka.serializers.KafkaAvroSerializer
import org.apache.avro.specific.SpecificRecord
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.clients.producer.ProducerConfig
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.kafka.KafkaProperties
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Profile
import org.springframework.kafka.core.*
import org.springframework.kafka.test.EmbeddedKafkaBroker
import org.springframework.kafka.test.utils.KafkaTestUtils

@TestConfiguration
@Profile("test")
internal class MockSerdeConfig (
    // KafkaProperties groups all properties prefixed with `spring.kafka`
    private val props: KafkaProperties) {

    /**
     * Mock schema registry bean used by Kafka Avro Serde since
     * the @EmbeddedKafka setup doesn't include a schema registry.
     *
     * @return MockSchemaRegistryClient instance
     */
    @Bean
    fun schemaRegistryClient() = MockSchemaRegistryClient()

    @Bean
    fun avroSerdeProvider(schemaRegistryClient: SchemaRegistryClient, @Value("\${spring.kafka.properties.schema.registry.url}") schemaRegistryUrls: String) : AvroSerdeProvider {
        return MockAvroSerdeProvider(schemaRegistryUrls, schemaRegistryClient)
    }

    /**
     * e.g. 'kafka1:9091,kafka2:9092'
     */
    @Bean
    fun kafkaBootstrapServerProvider(embeddedKafka: EmbeddedKafkaBroker) = embeddedKafka.brokersAsString

    /**
     * KafkaAvroSerializer that uses the MockSchemaRegistryClient
     *
     * @return KafkaAvroSerializer instance
     */
    @Bean
    fun kafkaAvroSerializer(registry: SchemaRegistryClient) = KafkaAvroSerializer(registry, props.buildProducerProperties())

    /**
     * KafkaAvroDeserializer that uses the MockSchemaRegistryClient.
     * The props must be provided so that specific.avro.reader: true
     * is set. Without this, the consumer will receive GenericData records.
     *
     * @return KafkaAvroDeserializer instance
     */
    @Bean
    fun kafkaAvroDeserializer(schemaRegistryClient: SchemaRegistryClient) = KafkaAvroDeserializer(schemaRegistryClient, props.buildConsumerProperties())

    /**
     * Configures the kafka producer factory to use the overridden
     * KafkaAvroDeserializer so that the MockSchemaRegistryClient
     * is used rather than trying to reach out via HTTP to a schema registry
     *
     * @return DefaultKafkaProducerFactory instance
     */
    @Bean
    fun producerFactory(serializer: KafkaAvroSerializer, embeddedKafkaBroker: EmbeddedKafkaBroker) : ProducerFactory<SpecificRecord, SpecificRecord> {
        val avroProps = KafkaTestUtils.producerProps(embeddedKafkaBroker)
        avroProps.replace(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer::class.java)
        avroProps.replace(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer::class.java)
        return DefaultKafkaProducerFactory(avroProps, serializer, serializer) as ProducerFactory<SpecificRecord, SpecificRecord>
    }

    /**
     * Configures the kafka consumer factory to use the overridden
     * KafkaAvroSerializer so that the MockSchemaRegistryClient
     * is used rather than trying to reach out via HTTP to a schema registry
     * @return DefaultKafkaConsumerFactory instance
     */
    @Bean
    fun consumerFactory(deserializer: KafkaAvroDeserializer, embeddedKafkaBroker: EmbeddedKafkaBroker, @Value("\${spring.kafka.consumer.group-id}") groupId: String) : ConsumerFactory<SpecificRecord, SpecificRecord> {
        val avroProps = KafkaTestUtils.consumerProps(groupId, "true", embeddedKafkaBroker)
        avroProps.replace(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, KafkaAvroSerializer::class.java)
        avroProps.replace(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, KafkaAvroSerializer::class.java)
        return DefaultKafkaConsumerFactory(avroProps, deserializer, deserializer) as ConsumerFactory<SpecificRecord, SpecificRecord>
    }
}
