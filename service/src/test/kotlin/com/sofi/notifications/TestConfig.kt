package com.sofi.notifications

import com.sofi.bouncer.api.BouncerSession
import org.davidmoten.rx.jdbc.Database
import org.jdbi.v3.core.Jdbi
import org.jdbi.v3.postgres.PostgresPlugin
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Primary
import org.springframework.context.annotation.Profile
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.RememberMeAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.authority.AuthorityUtils
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextImpl
import org.springframework.security.web.server.context.ServerSecurityContextRepository
import org.springframework.web.server.ServerWebExchange
import reactor.core.publisher.Mono
import java.util.*

@TestConfiguration
@Profile("test")
class TestConfig {

    /**
     * For the [com.sofi.notifications.legacy.web.LegacyControllerIntegrationTest]
     */
    @Bean
    @Primary
    fun jdbi(): Jdbi {
        return Jdbi.create(BaseIntegrationTest.preparedDbExtension.testDatabase)
                .installPlugin(PostgresPlugin())
    }

    @Bean
    @Primary
    fun db(): Database {
        return Database.fromBlocking(BaseIntegrationTest.preparedDbExtension.testDatabase)
    }

    //Mocks the security context and provides a bouncer session with the user ID specified in the test-user-id header
    @Bean
    @Primary
    fun testSecurityContextRepository(): ServerSecurityContextRepository {

        return object : ServerSecurityContextRepository {
            override fun save(exchange: ServerWebExchange, context: SecurityContext): Mono<Void> {
                return exchange.session
                        .flatMap { it.changeSessionId() }
            }

            override fun load(exchange: ServerWebExchange): Mono<SecurityContext> {
                val first = exchange.request.headers[testUserHeader]?.first()
                val bouncerSession = BouncerSession()
                return if (first != null) {
                    bouncerSession.partyId = first.toLong()
                    val authenticationToken = RememberMeAuthenticationToken(UUID.randomUUID().toString(), bouncerSession, AuthorityUtils.createAuthorityList())
                    Mono.justOrEmpty(SecurityContextImpl(authenticationToken))
                } else {
                    Mono.empty()
                }
            }
        }

    }

    @Bean
    @Primary
    fun testAuthenticationProvider(): AuthenticationProvider {
        return object : AuthenticationProvider {
            override fun supports(authentication: Class<*>?): Boolean {
                return true
            }

            override fun authenticate(authentication: Authentication): Authentication {
                return authentication
            }
        }
    }

    companion object {
        @JvmStatic
        val testUserHeader = "test-user-id"
    }
}
