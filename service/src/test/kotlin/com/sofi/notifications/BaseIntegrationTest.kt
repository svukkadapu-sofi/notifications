package com.sofi.notifications

import com.sofi.notifications.model.NotificationPageDto
import com.sofi.notifications.schemas.NotificationResult
import com.sofi.notifications.schemas.PartyIdKey
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.junit.jupiter.api.extension.RegisterExtension
import org.slf4j.LoggerFactory
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.kafka.annotation.EnableKafka
import org.springframework.kafka.test.context.EmbeddedKafka
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.reactive.server.WebTestClient
import javax.inject.Inject

typealias NotificationResultRecord = ConsumerRecord<PartyIdKey, NotificationResult>

@SpringBootTest(
        classes = [App::class, TestConfig::class, MockSerdeConfig::class],
        properties = ["spring.flyway.enabled=false"],
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@ActiveProfiles(profiles = ["test", "qa", "default"])
@EnableKafka
@EmbeddedKafka
@AutoConfigureWebTestClient // needed to autowire the webTestClient
internal class BaseIntegrationTest {

    @Inject
    lateinit var webTestClient: WebTestClient // initialized by Spring

    protected fun fetchNotificationPage(url: String, testUserId: String): NotificationPageDto? {
        return webTestClient.mutate().defaultHeader(TestConfig.testUserHeader, testUserId)
                .build()
                .get().uri(url)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus().isOk
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectBody(NotificationPageDto::class.java)
                .returnResult().responseBody
    }

    protected fun touchNotification(notificationId: String, testUserId: String) {
        webTestClient.mutate().defaultHeader(TestConfig.testUserHeader, testUserId)
                .build()
                .put().uri("/notifications/$notificationId/touch")
                .exchange()
                .expectStatus().isOk
    }

    companion object {
        @JvmField
        @RegisterExtension
        val preparedDbExtension = PreparedDbExtension.preparedDatabase()

        @JvmField
        @RegisterExtension
        val springExtension = SpringExtension() // starts spring

        private val log = LoggerFactory.getLogger(BaseIntegrationTest::class.java)
    }
}
