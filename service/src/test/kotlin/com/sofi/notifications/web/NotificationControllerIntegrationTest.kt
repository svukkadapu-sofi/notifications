package com.sofi.notifications.web

import com.sofi.notifications.BaseIntegrationTest
import com.sofi.notifications.TestConfig.Companion.testUserHeader
import com.sofi.notifications.db.NotificationEntity
import com.sofi.notifications.model.CountDto
import org.davidmoten.rx.jdbc.Database
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType
import org.springframework.test.web.reactive.server.WebTestClient
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import javax.inject.Inject

internal class NotificationControllerIntegrationTest : BaseIntegrationTest() {

    @Inject
    lateinit var db: Database

    @Test
    fun `test first page of notifications endpoint`() {
        val page = fetchNotificationPage("/notifications?count=10", "1")

        assertNotNull(page)

        //assert the next url
        assertNotNull(page?.next)
        assertEquals("/notifications?count=10&after=769adc12-789b-4d8f-a129-b34759f6fc87", page?.next)

        //assert the returned notifications
        assertNotNull(page?.notifications)
        assertEquals(10, page?.notifications?.size)

        //assert the order (reverse chronological)
        assertEquals("5757bc56-fb2d-4e14-b7ca-16dfedd0e069", page?.notifications!![0].id)
        assertEquals("5a118fe2-6244-4cb8-9f50-bf3d04256c32", page.notifications[1].id)
        assertEquals("423bdb67-1c88-427a-a5db-8efce7b5aa43", page.notifications[2].id)
        assertEquals("87a0d7da-46b5-420d-94ef-238bdbf6326c", page.notifications[3].id)
        assertEquals("aca9bd5a-bb20-486b-a579-4ebed48dc0db", page.notifications[4].id)
        assertEquals("babef9b9-930b-4b15-8b2b-d8ffcc58f0c5", page.notifications[5].id)
        assertEquals("fa19e509-b3c9-4ff9-8318-bb77229ad8d4", page.notifications[6].id)
        assertEquals("ef071873-f700-405b-95e1-a9c0b5fc99b6", page.notifications[7].id)
        assertEquals("03d1ae21-3f3a-473a-b4fa-19aac48a58b5", page.notifications[8].id)
        assertEquals("769adc12-789b-4d8f-a129-b34759f6fc87", page.notifications[9].id)
    }

    @Test
    fun `test second page of notifications endpoint`() {
        val page = fetchNotificationPage("/notifications?count=10&after=769adc12-789b-4d8f-a129-b34759f6fc87", "1")

        assertNotNull(page)

        //assert the next url
        assertNull(page?.next)

        //assert the returned notifications
        assertNotNull(page?.notifications)
        assertEquals(10, page?.notifications?.size)

        //assert the order (reverse chronological)
        assertEquals("1aac7709-a773-4fd0-ad9f-4b31db8ce706", page?.notifications!![0].id)
        assertEquals("d53e2341-1a8d-4f20-a2e8-b6155752d3f1", page.notifications[1].id)
        assertEquals("767a3f2d-87e4-4e19-b3c5-aa98058760e1", page.notifications[2].id)
        assertEquals("4d17302c-b59e-447a-b2a9-d57931cbbec1", page.notifications[3].id)
        assertEquals("40b03b58-92e5-4086-9ef2-5c4811f1b271", page.notifications[4].id)
        assertEquals("88dbbab5-3204-4cb2-981a-ce8f43547e87", page.notifications[5].id)
        assertEquals("71d6c23e-35b5-446b-9d19-cf3f1ac061fd", page.notifications[6].id)
        assertEquals("19d66d15-87a5-4f46-8a81-da910f86ad32", page.notifications[7].id)
        assertEquals("03aeb40c-c833-46fe-a706-8559bd0980df", page.notifications[8].id)
        assertEquals("6b34847e-61ce-48db-9496-83f9ecf82960", page.notifications[9].id)
    }

    @Test
    fun `count unseen endpoint exists`() {
        getUnseenNotifications("20181114")
                .expectStatus().isOk
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectBody(CountDto::class.java)
    }

    @Test
    fun `count unseen endpoint returns the number of new notifications`() {
        val entityExchangeResult = getUnseenNotifications("20181114")
                .expectStatus().isOk
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectBody(CountDto::class.java)
                .returnResult()

        assertNotNull(entityExchangeResult.responseBody?.count)
        assertEquals(5L, entityExchangeResult.responseBody?.count)
    }

    @Test
    fun `count unseen endpoint returns zero when there are no messages`() {
        val entityExchangeResult = getUnseenNotifications("20181113")
                .expectStatus().isOk
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectBody(CountDto::class.java)
                .returnResult()

        assertNotNull(entityExchangeResult.responseBody?.count)
        assertEquals(0L, entityExchangeResult.responseBody?.count)
    }

    private fun getUnseenNotifications(partyId: String): WebTestClient.ResponseSpec {
        return webTestClient
                .mutate().defaultHeader(testUserHeader, partyId)
                .build()
                .get().uri("/notifications/count/unseen")
                .exchange()
    }

    @Test
    fun `test mark all as read`() {
        //mark the notifications as read
        webTestClient.mutate().defaultHeader(testUserHeader, "10")
                .build()
                .put().uri("/notifications/markallasread")
                .exchange()
                .expectStatus().isOk

        //check the database has been correctly updated
        val notifications = Flux.from(db.select("SELECT * FROM notification WHERE party_id = 10")
                .autoMap(NotificationEntity::class.java)).collectList()
                .block()

        assertEquals(5, notifications?.size)
        for (n in notifications!!) {
            assertEquals("READ", n.status())
        }
    }

    @Test
    fun `test touch notification with null web_url and null native_path`() {
        executeAndAssertNotificationTouch("11", "c2836ff8-c2ef-4e49-8d07-e1c11ad73e41", "READ")
    }

    @Test
    fun `test touch notification with non null web_url and null native_path`() {
        executeAndAssertNotificationTouch("11", "c2836ff8-c2ef-4e49-8d07-e1c11ad73e42", "ACTION_TAKEN")
    }

    @Test
    fun `test touch notification with null web_url and non null native_path`() {
        executeAndAssertNotificationTouch("11", "c2836ff8-c2ef-4e49-8d07-e1c11ad73e43", "ACTION_TAKEN")
    }

    @Test
    fun `test touch notification with non null web_url and non null native_path`() {
        executeAndAssertNotificationTouch("11", "c2836ff8-c2ef-4e49-8d07-e1c11ad73e44", "ACTION_TAKEN")
    }

    private fun executeAndAssertNotificationTouch(testUserId: String, notificationId: String, expectedStatus: String) {
        //mark the notifications as read
        touchNotification(notificationId, testUserId)

        //check the database has been correctly updated
        val notification = Mono.from(db.select("SELECT * FROM notification WHERE notification_id = '$notificationId'")
                .autoMap(NotificationEntity::class.java)).block()

        assertNotNull(notification)
        assertEquals(expectedStatus, notification?.status())
    }
}
