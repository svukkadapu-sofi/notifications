package com.sofi.notifications.legacy.repository

import com.sofi.notifications.PreparedDbExtension
import com.sofi.notifications.legacy.*
import org.jdbi.v3.core.Jdbi
import org.jdbi.v3.postgres.PostgresPlugin
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension
import reactor.core.publisher.Flux
import reactor.test.StepVerifier

internal class LegacyPostgresNotificationRepositoryTest {

    private val repository = LegacyPostgresNotificationRepository(
            Jdbi.create(preparedDbExtension.testDatabase).installPlugin(PostgresPlugin())
    )

    @Test
    fun getPage() {
        // proof that the database migrations work
        val maybeNotification = repository.getPage(10000, 0, 5).blockFirst()

        assertNull(maybeNotification) // because there's nothing in the database at this point.
    }

    @Test
    fun hasNew_true() {
        val testId = randomCustomerId()
        repository.createTestNotification(testId, "Test Message")

        StepVerifier.create(repository.hasNew(testId))
                .expectNext(HasNewStatus(testId, true))
                .verifyComplete()
    }

    @Test
    fun hasNew_false() {
        val testId = randomCustomerId()

        StepVerifier.create(repository.markAllAsRead(testId)
                .then(repository.hasNew(testId)))
                .expectNext(HasNewStatus(testId, false))
                .verifyComplete()
    }

    @Test
    fun createAndGetOneNotification() {

        val testId = randomCustomerId()

        val notificationId = repository.createTestNotification(testId, "Hello, World")

        StepVerifier.create(repository.getOneNotification(testId, notificationId))
                .expectNextMatches {
                    it.message == "Hello, World" &&
                            it.id == notificationId &&
                            it.partyId == testId &&
                            it.status == NotificationStatus.NEW
                }
                .verifyComplete()
    }

    @Test
    fun updateSingleStatus() {

        val testId = randomCustomerId()

        val notificationId = repository.createTestNotification(testId, "Foo")

        val notificationBefore = repository.getOneNotification(testId, notificationId).block()!!

        StepVerifier.create(repository.updateSingleStatus(testId, notificationId, NotificationStatus.UNREAD)
                .then(repository.getOneNotification(testId, notificationId)))
                .expectNextMatches {
                    it.copy(status = NotificationStatus.NEW) == notificationBefore &&
                            it.status == NotificationStatus.UNREAD
                }
                .verifyComplete()
    }

    @Test
    fun markAllAsRead() {
        val testId = randomCustomerId()

        val createdIds = Array(5) { repository.createTestNotification(testId, "Message $it") }

        assertTrue(repository.hasNew(testId).block()!!.hasNew, "There should be new notifications")

        // mark all as read then verify that "hasNew" returns "false"
        StepVerifier.create(repository.markAllAsRead(testId)
                .then(repository.hasNew(testId)))
                .expectNextMatches { !it.hasNew }
                .verifyComplete()

        // let's pull out all the notifications that were modified
        val markedNotifications = createdIds.map { repository.getOneNotification(testId, it) }

        // then let's combine all those async operations (like JS's Promise.all()), confirming the status of each as it comes in
        val allAreMarkedAsRead = Flux.combineLatest(markedNotifications) { arrayOfObjects ->
            arrayOfObjects.map { (it as Notification).status == NotificationStatus.READ }.all { it }
        }.blockFirst()

        assertNotNull(allAreMarkedAsRead) // the block should, effectively, return a filled optional
        assertTrue(allAreMarkedAsRead!!)
    }

    companion object {
        @JvmField
        @RegisterExtension
        val preparedDbExtension = PreparedDbExtension.preparedDatabase()

        fun randomCustomerId(): CustomerId = kotlin.random.Random.nextInt()
    }
}
