package com.sofi.notifications.legacy

import com.sofi.notifications.legacy.repository.NotificationRepository

fun NotificationRepository.createTestNotification(id: CustomerId, message: String): NotificationId {
    return this.createNotification(id, CreateNotificationPayload(message, CallToAction("text", "/url"))).block()!!
}
