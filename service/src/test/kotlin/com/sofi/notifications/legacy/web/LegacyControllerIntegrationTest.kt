package com.sofi.notifications.legacy.web

import com.sofi.notifications.BaseIntegrationTest
import com.sofi.notifications.legacy.*
import com.sofi.notifications.legacy.repository.NotificationRepository
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType
import org.springframework.test.web.reactive.server.body
import reactor.core.publisher.Mono
import javax.inject.Inject

internal class LegacyControllerIntegrationTest : BaseIntegrationTest() {

    @Inject
    lateinit var notificationRepository: NotificationRepository

    private val testCustomerId = 1000158

    @Test
    fun getPageOfNotifications() {

        val pageLength = 5
        val pageNumber = 0

        webTestClient
                .get().uri("/customers/$testCustomerId?pageLength=$pageLength&pageNumber=$pageNumber")
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus().isOk
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectBodyList(Notification::class.java)
    }

    @Test
    fun doesCustomerHaveNewNotifications() {

        // Create a new notification
        val payload = CreateNotificationPayload("has new message", null)
        notificationRepository.createNotification(testCustomerId, payload).block()

        // Test the API. It should agree that there IS a new notification
        webTestClient
                .get().uri("/customers/$testCustomerId/has-new")
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus().isOk
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectBody(HasNewStatus::class.java)
    }

    @Test
    fun getOneNotification() {

        val testCreateMessage = "Message"
        val expectedResponse = notificationRepository.createTestNotification(testCustomerId, testCreateMessage)

        val entityExchangeResult = webTestClient.get().uri("/customers/$testCustomerId/notifications/$expectedResponse")
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus().isOk
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectBody(Notification::class.java)
                .returnResult()

        Assertions.assertNotNull(entityExchangeResult.responseBody)
        Assertions.assertNotNull(entityExchangeResult.responseBody?.message)
        Assertions.assertEquals(testCreateMessage, entityExchangeResult.responseBody?.message)
    }

    @Test
    fun createNotification() {

        val testCreatePayload = CreateNotificationPayload("Hey all you people", CallToAction("Get Help", "/help"))

        webTestClient.post().uri("/customers/$testCustomerId")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .body(Mono.just(testCreatePayload), CreateNotificationPayload::class.java)
                .exchange()
                .expectStatus().isOk
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectBody(NotificationId::class.java)
    }

    @Test
    fun markAllNotificationsAsRead() {

        notificationRepository.createTestNotification(testCustomerId, "Mark me as read, cap'n")

        webTestClient.put().uri("/customers/$testCustomerId")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus().isOk
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectBody()
                .jsonPath("$.succeeded").isEqualTo(true)
    }

    @Test
    fun markOneNotificationAsUnread() {
        val notificationId = notificationRepository.createTestNotification(testCustomerId, "Message")

        webTestClient.put().uri("/customers/$testCustomerId/notifications/$notificationId")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .body(Mono.just("""{ "status": "UNREAD" }"""))
                .exchange()
                .expectStatus().isOk
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectBody()
                .jsonPath("$.success").isEqualTo(true)
                .jsonPath("$.newStatus").isEqualTo("UNREAD")
    }

    @Test
    fun markOneNotificationAsRead() {
        val notificationId = notificationRepository.createTestNotification(testCustomerId, "Message")

        webTestClient.put().uri("/customers/$testCustomerId/notifications/$notificationId")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .body(Mono.just("""{ "status": "READ" }"""))
                .exchange()
                .expectStatus().isOk
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectBody()
                .jsonPath("$.success").isEqualTo(true)
                .jsonPath("$.newStatus").isEqualTo("READ")
    }

    @Test
    fun `mark a previously UNREAD notification as NEW fails`() {
        val notificationId = notificationRepository.createTestNotification(testCustomerId, "Message")

        // make our test notification UNREAD, past the point it should be able to be NEW
        notificationRepository.updateSingleStatus(testCustomerId, notificationId, NotificationStatus.UNREAD).block()

        // test the API, asking it to go backward (UNREAD -> NEW), and observing the failure
        webTestClient.put().uri("/customers/$testCustomerId/notifications/$notificationId")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .body(Mono.just("""{ "status": "NEW" }"""))
                .exchange()
                .expectStatus().is5xxServerError
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectBody()
                .jsonPath("$.message").exists()
                .jsonPath("$.status").isEqualTo(500)
    }
}
