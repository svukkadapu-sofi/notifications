package com.sofi.notifications.model

import com.sofi.notifications.db.NotificationEntity
import java.time.Instant
import java.util.*
import kotlin.random.Random

class TestNotificationEntity: NotificationEntity {

    private val _now = Instant.now()
    private val _message = "Mock Notification"
    private val _partyId = Random.nextLong(100_000)
    private val _notificationId = lazy { UUID.randomUUID() }
    private val _communicationId = lazy { UUID.randomUUID().toString() }


    override fun notificationId(): UUID = _notificationId.value

    override fun partyId(): Long = _partyId

    override fun communicationId(): String = _communicationId.value

    override fun message(): String = _message

    override fun status(): String = "some Status"

    override fun nativePath(): String? = "/Money/Invites"

    override fun webUrl(): String? = "www.sofi.com"

    override fun eventDate(): Instant = _now

    override fun urgent(): Boolean = false

    override fun origin(): String = "yo momma"

    override fun toString(): String {
        return """
            TestNotification{
                notificationId = ${notificationId()},
                partyId = ${partyId()},
                communicationId = ${communicationId()},
                eventDate = ${eventDate()},
                message = ${message()}
            }
        """.trimIndent()
    }
}
