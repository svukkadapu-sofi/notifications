package com.sofi.notifications.model

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class AvroSchemaMappersKtTest {

    @Test
    fun toNotificationResult() {
        // instantiation should not fail
        val backingNotification = TestNotificationEntity()
        val kafkaReadyInstance = backingNotification.toNotificationResult()

        assertEquals(backingNotification.eventDate().toEpochMilli(), kafkaReadyInstance.eventTimestamp.toInstant().millis,
                "The event data should be the same even though their types aren't byte-equivalent")

        assertEquals(backingNotification.status(), kafkaReadyInstance.status,
                "The notification status should be the same after kafka-readying")

        assertEquals(backingNotification.partyId(), kafkaReadyInstance.customerId,
                "The partyId should be the same after kafka-readying")

        assertEquals(backingNotification.communicationId(), kafkaReadyInstance.communicationId,
                "The communicationId should be the same after kafka-readying")
    }

}