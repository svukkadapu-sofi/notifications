package com.sofi.notifications.service

import com.sofi.kafka.serialization.AvroDeserializer
import com.sofi.kafka.serialization.AvroSerializer
import com.sofi.notifications.BaseKafkaTest
import com.sofi.notifications.config.kafka.StreamsConfigProvider
import com.sofi.notifications.model.NotificationDto
import com.sofi.notifications.model.TestNotificationEntity
import com.sofi.notifications.schemas.CreateNotificationRequest
import com.sofi.notifications.schemas.Metadata
import com.sofi.notifications.schemas.PartyIdKey
import com.sofi.notifications.topics.NotificationCenterTopics.`CreateNotificationRequest$topicName`
import io.confluent.kafka.schemaregistry.client.MockSchemaRegistryClient
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.apache.kafka.common.serialization.Serde
import org.apache.kafka.common.serialization.Serdes
import org.apache.kafka.streams.StreamsConfig
import org.apache.kafka.streams.TopologyTestDriver
import org.apache.kafka.streams.test.ConsumerRecordFactory
import org.joda.time.DateTime
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import reactor.core.publisher.Mono

internal class CreateNotificationRequestConsumerTest : BaseKafkaTest() {

    val springConfigProvider: StreamsConfigProvider = mockk {
        every { buildStreamsConfig(any()) } returns mapOf(
                StreamsConfig.APPLICATION_ID_CONFIG to "foo application id",
                StreamsConfig.BOOTSTRAP_SERVERS_CONFIG to "foo bootstrap server urls"
        ).toProperties()
    }

    @Test
    fun `PartyId is data-consistent through serialization`() {
        val testIdKey = PartyIdKey(1L)
        val bytesOfPartyId = partyIdSerdes.serializer().serialize(topic, testIdKey)

        val deserializedIdKey = partyIdSerdes.deserializer().deserialize(topic, bytesOfPartyId)

        assertEquals(testIdKey, deserializedIdKey, "The party ids are the same")
        assertEquals(testIdKey.customerId, deserializedIdKey.customerId, "The backing long-ints should be equal")
    }

    @Test
    fun `CreateNotificationRequest is data-consistent through serialization`() {
        val notificationRequest = generateTestCreateNotificationRequest()
        val bytesOfPartyId = createNotificationSerdes.serializer().serialize(topic, notificationRequest)

        // Note: the "topic" passed to deserialize(topic: String, data: ByteArray) does __not__ matter.
        val deserializedCreateRequest = createNotificationSerdes.deserializer().deserialize("foo bar", bytesOfPartyId)

        assertEquals(notificationRequest, deserializedCreateRequest, "The notification requests are the same")
    }

    @Nested
    inner class IntegrationTest {
        /**
         * Use [TopologyTestDriver] to interact with the underlying [org.apache.kafka.streams.kstream.KStream] that we use for consumption
         */
        @Test
        fun `Topology calls NotificationService once per CreateNotificationRequest message`() {

            // The service that is actually under test
            val mockNotificationService = mockk<NotificationService> {
                // every call to `createNotification`, no matter the parameters, should resolve a TestNotificationEntity
                every { createNotification(any(), any(), any(), any(), any(), any(), any(), any()) } returns Mono.just(NotificationDto.fromEntity(TestNotificationEntity()))
            }
            val consumerService = CreateNotificationRequestConsumer(mockNotificationService, springConfigProvider, partyIdSerdes, createNotificationSerdes)
            val testDriver = TopologyTestDriver(
                    consumerService.buildTopology(),
                    springConfigProvider.buildStreamsConfig("test id suffix")
            )

            // This produces Kafka-valid records for consumption
            val factory = ConsumerRecordFactory(partyIdSerdes.serializer(), createNotificationSerdes.serializer())
            val partyIdKey = PartyIdKey(1)
            // we make a thing to send through Kafka
            val record = partyIdKey.let { factory.create(`CreateNotificationRequest$topicName`, it, generateTestCreateNotificationRequest(it)) }

            // Send the input through our topology
            testDriver.pipeInput(record)

            // createNotification should be called exactly once, with any parameters
            verify(exactly = 1) { mockNotificationService.createNotification(any(), any(), any(), any(), any(), any(), any(), any()) }
        }
    }

    companion object {
        private const val topic = `CreateNotificationRequest$topicName`

        // The test deserializers append "-value" to the topic name... I don't know why
        private const val confusingSubjectName = "$`CreateNotificationRequest$topicName`-value"

        private val mockClient = MockSchemaRegistryClient().apply {
            this.register(confusingSubjectName, PartyIdKey.getClassSchema())
            this.register(confusingSubjectName, CreateNotificationRequest.getClassSchema())
        }

        private fun generateTestCreateNotificationRequest(partyIdKey: PartyIdKey = PartyIdKey(1)): CreateNotificationRequest {
            val testMetadata = Metadata("here", DateTime.now(), true)
            return CreateNotificationRequest("commId1", partyIdKey.customerId, "test body", "some/native/path", "www.sofi.com", testMetadata)
        }

        // needed for serializers and deserializers to be used by the stream
        private val essentialAvroConfig = mapOf("schema.registry.url" to "some url", "specific.avro.reader" to true)

        private val partyIdSerdes = testSerdes<PartyIdKey>()

        private val createNotificationSerdes = testSerdes<CreateNotificationRequest>()

        private fun <T> testSerdes(client: MockSchemaRegistryClient = mockClient): Serde<T> {
            return Serdes.serdeFrom(
                    AvroSerializer<T>(client).apply { configure(essentialAvroConfig, false) },
                    AvroDeserializer<T>(client).apply { configure(essentialAvroConfig, false) }
            )
        }
    }

}
