package com.sofi.notifications.service

import com.sofi.notifications.BaseKafkaTest
import com.sofi.notifications.NotificationResultRecord
import com.sofi.notifications.TestConfig.Companion.testUserHeader
import com.sofi.notifications.topics.NotificationCenterTopics
import org.hamcrest.CoreMatchers.containsString
import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert.assertThat
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test


internal class NotificationResultIntegrationTest : BaseKafkaTest() {

    @Test
    fun `unseen events are evented when notifications page is fetched`() {

        val page = fetchNotificationPage("/notifications?count=10", "12")

        assertNotNull(page)

        //assert the returned notifications
        assertNotNull(page?.notifications)
        assertEquals(2, page?.notifications?.size)

        val sentRecords: List<NotificationResultRecord> = getSentRecords(NotificationCenterTopics.`NotificationResult$topicName`)

        assertEquals(2, sentRecords.size) //matches the number of UNSEEN notifications for user 12
        sentRecords.forEach {
            assertEquals(12L, it.key().customerId)
            assertThat(it.value().communicationId, containsString("commsID12"))
            assertThat(it.value().status, equalTo("UNREAD"))
        }
    }

    @Test
    fun `marking all as read moves "UNREAD" to "READ" and we get a Kafka event`() {
        val myTestUserId: Long = 14
        // mark all as UNREAD
        webTestClient.mutate().defaultHeader(testUserHeader, "$myTestUserId")
                .build()
                .put()
                .uri("/notifications/markallasread")
                .exchange()
                .expectStatus().is2xxSuccessful

        val sentRecords: List<NotificationResultRecord> = getSentRecords(NotificationCenterTopics.`NotificationResult$topicName`)
        assertEquals(2, sentRecords.size)

        sentRecords.forEach {
            assertEquals(myTestUserId, it.key().customerId)
            assertThat(it.value().communicationId, containsString("commsID14"))
            assertThat(it.value().status, equalTo("READ"))
        }
    }

    @Test
    fun `notifications are evented when touched`() {

        touchNotification("c2836ff8-c2ef-4e49-8d07-e1c11ad73e4a", "13")
        touchNotification("c2836ff8-c2ef-4e49-8d07-e1c11ad73e4b", "13")
        //touch this one twice ;)
        touchNotification("c2836ff8-c2ef-4e49-8d07-e1c11ad73e4c", "13")
        touchNotification("c2836ff8-c2ef-4e49-8d07-e1c11ad73e4c", "13")

        val sentRecords: List<NotificationResultRecord> = getSentRecords(NotificationCenterTopics.`NotificationResult$topicName`)

        //check that each records is for th correct user and has the correct comms ID
        assertEquals(4, sentRecords.size) //matches the number of UNSEEN notifications for user 12
        sentRecords.forEach {
            assertEquals(13L, it.key().customerId)
            assertThat(it.value().communicationId, containsString("commsID13"))
        }

        //Check the first notification
        val firstNotification = sentRecords.find { it.value().communicationId == "commsID13_1" }
        assertNotNull(firstNotification)
        assertEquals("ACTION_TAKEN", firstNotification!!.value().status)

        //Check the second notification
        val secondNotification = sentRecords.find { it.value().communicationId == "commsID13_2" }
        assertNotNull(secondNotification)
        assertEquals("ACTION_TAKEN", secondNotification!!.value().status)

        //Check that the third notification evented twice
        val thirdNotifications = sentRecords.filter { it.value().communicationId == "commsID13_3" }
        assertNotNull(thirdNotifications)
        assertEquals(2, thirdNotifications.size)
        thirdNotifications.forEach { assertEquals("ACTION_TAKEN", it.value().status) }
    }
}
