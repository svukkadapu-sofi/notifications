package com.sofi.notifications.service

import com.sofi.avro.schemas.AppBadgeCount
import com.sofi.avro.schemas.PartyId
import com.sofi.firebase.topics.FirebaseTopics
import com.sofi.notifications.BaseKafkaTest

import com.sofi.notifications.db.NotificationRepository
import com.sofi.notifications.db.PostgresNotificationRepository
import com.sofi.notifications.db.toDto
import com.sofi.notifications.model.TestNotificationEntity
import org.apache.avro.specific.SpecificRecord
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.junit.jupiter.api.Assertions

import org.junit.jupiter.api.Test
import org.mockito.Mockito.*
import org.springframework.kafka.core.KafkaTemplate
import reactor.core.publisher.Mono
import reactor.test.StepVerifier

import java.time.Instant
import org.junit.jupiter.api.BeforeEach
import javax.inject.Inject

internal class NotificationServiceTest : BaseKafkaTest()  {

    @Inject
    val kafkaTemplate: KafkaTemplate<SpecificRecord, SpecificRecord>? = null

    @Inject
    private lateinit var notificationResultProducer: NotificationResultProducer

    private var notificationService: NotificationService? = null

    private var mockRepository: NotificationRepository? = null

    private var badgeCountProducer: BadgeCountProducer? = null

    @BeforeEach
    fun setupMock() {
                // wait until the partitions are assigned
        mockRepository = mock(PostgresNotificationRepository::class.java)
        badgeCountProducer = BadgeCountProducer(kafkaTemplate!!, mockRepository!!)
        notificationService = NotificationService(mockRepository!!, notificationResultProducer, badgeCountProducer!!)
    }

    @Test
    fun test() {
        // given
        val entity = TestNotificationEntity()
        val now = Instant.now()
        `when`(mockRepository!!.createNotification(1, "", "", "", "", now, false, "")).thenReturn(Mono.just(entity))
        `when`(mockRepository!!.fetchUnseenNotificationsCount(1)).thenReturn(Mono.just(1))

        // when
        val done = notificationService!!.createNotification(1, "", "", "", "", now, false, "")

        // then
        StepVerifier.create(done)
                .expectNext(entity.toDto())
                .verifyComplete()
//
        val sentRecords: List<ConsumerRecord<PartyId, AppBadgeCount>> = getSentRecords(FirebaseTopics.`AppBadgeCount$topicName`)

        Assertions.assertEquals(1, sentRecords.size) //matches the number of UNSEEN notifications for user 12
        sentRecords.forEach {
            Assertions.assertEquals(1L, it.key().partyId)
            Assertions.assertEquals(1, it.value().badgeCount)
        }
    }
}
