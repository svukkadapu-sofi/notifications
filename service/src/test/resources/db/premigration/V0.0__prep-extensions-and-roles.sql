-- prerequisites for the flyway scripts as they are :(
CREATE USER notifications;
CREATE ROLE notificationsapprole;
CREATE EXTENSION pgcrypto SCHEMA pg_catalog; -- for PostgreSQL 'uuid's
CREATE EXTENSION pg_trgm SCHEMA pg_catalog; -- for GIN index in general
CREATE EXTENSION btree_gin schema pg_catalog; -- for GIN index on bigint
