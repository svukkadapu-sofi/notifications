#!/bin/bash

dbProjectName="notifications-db"

dbRepo="git@bitbucket.org:sofiinc/$dbProjectName.git"

function checkDbBranch() {
        echo "Checking if branch, $1, exists in $dbRepo"
        git ls-remote --heads $dbRepo $1 | grep $1 >/dev/null
        return "$?"
}

function usingMaster() {
    echo "Using master DB branch"
}

function dbBranchDoesNotExist() {
    echo "Branch, $1, does not exist in $dbRepo"
}

dbBranch="master"
if [[ $# -eq 0 ]] ; then
    currentBranch="$(git symbolic-ref --short -q HEAD)"
    if [ $currentBranch == "master" ] ; then
        usingMaster
    else
        checkDbBranch $currentBranch
        if [ "$?" != 0 ] ; then
            dbBranchDoesNotExist $currentBranch
            usingMaster
        else
            echo "Using DB branch with same name as this current branch: $currentBranch"
            dbBranch=$currentBranch
        fi
    fi
else
    if [ $1 == "master" ] ; then
        usingMaster
    else
        checkDbBranch $1
        if [ "$?" == 0 ] ; then
            echo "Using DB branch, $1"
            dbBranch=$1
        else
            dbBranchDoesNotExist $1
            echo "Failed to clone DB repo, $1"
            usingMaster
        fi
    fi
fi

clonedDbPath="$buildDir/tmp/$dbProjectName"
dbMigrationSourceDir="$clonedDbPath/src/main/resources/db"
tmpMigrationTargetDir="src/test/generated-resources"
dbMigrationTargetDir="$tmpMigrationTargetDir/db"

rm -rf $clonedDbPath
git clone -b $dbBranch --single-branch --depth 1 $dbRepo $clonedDbPath
rm -rf $dbMigrationTargetDir
mkdir -p $tmpMigrationTargetDir
cp -rf $dbMigrationSourceDir $dbMigrationTargetDir