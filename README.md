# Notifications Service

This Readme specifies what we set out to do and references all documentation regards how we did it.

## Introduction

A micro-service backing the Notification Center, and a future member of the Notifications Platform

## Project Objectives

Build a self-healing, redundant system to track, update, and transmit user/borrower notifications
through the

## Design Specifications & Flow Diagrams

Please see the [documentation on Confluence](https://sofiinc.atlassian.net/wiki/spaces/ENG/pages/195907750/Notification+Service+Documentation)

## Project Timeline

### Initial release (v1)

We hope to release in the late of Q4-17, or first thing in Q1-18

## QA Tools

We're adding a QAController with the following endpoints. To enable it locally, add the environment variable SPRING_PROFILES_ACTIVE and set it to "default,qa"

### Notification Results Tests

* POST /qa/notifications with an X-Sofi-Bouncer-V2 header and the following form-data fields
  * message: String __required__
  * nativePath: String _optional_
  * webUrl: String _optional_
  * urgent: true|false _optional_ (defaults to false)
  * read: true|false _optional_ (defaults to false)

To test notifications. You can do the following:
1. Log into dashboard

2. Open another tab in your browser and go to [/notifications/](/notifications/)
    * if it's a new user you'll see no notification, if it's not you may have notificaitons

3. Next you'll need a valid **X-Sofi-Bouncer-V2 token**. To get that:
    * In your browser, copy the Cookie value for the [/notifications](/notifications) request
    * In postman make a **GET** request for [kraken-qa-[x]:9004/bouncer/api/v1/access?authorize=BORROWER](kraken-qa-[x]:9004/bouncer/api/v1/access?authorize=BORROWER) with the Cookie copied in the previous step added to your request headers
    * Look at the result headers and copy the **X-Sofi-Bouncer-V2 token**

4. Create some notifications with the **QA controller tool** we've created for you _(this will not be available in prod)_.
    * Use postman to make a **POST** request to [kraken-qa-[x]:9175/qa/notifications](kraken-qa-[x]:9175/qa/notifications) with the X-Sofi-Bouncer-V2 token header copied previously
        * For data in the post, use the **Body** tab and form-data selected. You can pass the following values:
            * message: String __required__
            * nativePath: String _optional_
            * webUrl: String _optional_
            * urgent: true|false _optional_ (defaults to false)
            * read: true|false _optional_ (defaults to false)

5. Install the confluent tools for testing using `brew install confluent-oss`

6. Use the confluent `kafka-avro-console-consumer` tool to make sure that creation of notifications has the following results
    * When notifications are created there should be a message produced to the `domain.notifications.event.notification_action` topic with the status of **UNSEEN**
    * When the **GET** request to [/notifications/](/notifications/) is hit and the notifications are presented to the user, each unseen notification should now become unread and a message should be produced on the  `domain.notifications.event.notification_action` topic with the status of **UNREAD**
    * When a **PUT** request to  [kraken-qa-[x]:9175/notifications/[notifications-id]/touch](kraken-qa-[x]:9175/notifications/[notifications-id]/touch) endpoint is hit  an individual message will be published indicating that the notification has been **READ**
    * When a **PUT** request to [kraken-qa-[x]:9175/notifications/markallasread](kraken-qa-[x]:9175/notifications/markallasread) is hit a message for each unread notification should be produced on the `domain.notifications.event.notification_action` topic with the status of **UNREAD**

7. Witness that each of those cases happen by using the `kafka-avro-console-consumer` tool
```bash
kafka-avro-console-consumer --bootstrap-server kraken-qa-[x]:9092 --property schema.registry.url=http://kraken-qa-[x]:8081 --topic domain.notifications.event.notification_action --from-beginning
```

### App Badge Count update Test

To test notifications badge count changes. You can do the following:
1. Log into dashboard

2. Open another tab in your browser and go to [/notifications/](/notifications/)
    * if it's a new user you'll see no notification, if it's not you may have notificaitons

3. Next you'll need a valid **X-Sofi-Bouncer-V2 token**. To get that:
    * In your browser, copy the Cookie value for the [/notifications](/notifications) request
    * In postman make a **GET** request for [kraken-qa-[x]:9004/bouncer/api/v1/access?authorize=BORROWER](kraken-qa-[x]:9004/bouncer/api/v1/access?authorize=BORROWER) with the Cookie copied in the previous step added to your request headers
    * Look at the result headers and copy the **X-Sofi-Bouncer-V2 token**

4. Create some notifications with the **QA controller tool** we've created for you _(this will not be available in prod)_.
    * Use postman to make a **POST** request to [kraken-qa-[x]:9175/qa/notifications](kraken-qa-[x]:9175/qa/notifications) with the X-Sofi-Bouncer-V2 token header copied previously
        * For data in the post, use the **Body** tab and form-data selected. You can pass the following values:
            * message: String __required__
            * nativePath: String _optional_
            * webUrl: String _optional_
            * urgent: true|false _optional_ (defaults to false)
            * read: true|false _optional_ (defaults to false)

5. Install the confluent tools for testing using `brew install confluent-oss`

6. Use the confluent `kafka-avro-console-consumer` tool to make sure that creation of notifications has the following results
    * When notifications are created there should be a message produced to the following topics:
        * `domain.notifications.event.notification_action` topic with the status of **UNSEEN**
        * `com.sofi.avro.schemas.AppBadgeCount` topic with an updated unseen notifications count for the user
    * When the **GET** request to [/notifications/](/notifications/) is hit and the notifications are presented to the user, each unseen notification should now become unread and a message should be produced to the following topics:
        * `domain.notifications.event.notification_action` topic with the status of **UNREAD**
        * `com.sofi.avro.schemas.AppBadgeCount` topic with an updated unseen notifications count for the user

7. Witness that each of those cases happen by using the `kafka-avro-console-consumer` tool
```bash
kafka-avro-console-consumer --bootstrap-server kraken-qa-[x]:9092 --property schema.registry.url=http://kraken-qa-[x]:8081 --topic com.sofi.avro.schemas.AppBadgeCount --from-beginning
```
```bash
kafka-avro-console-consumer --bootstrap-server kraken-qa-[x]:9092 --property schema.registry.url=http://kraken-qa-[x]:8081 --topic domain.notifications.event.notification_action --from-beginning
```

### Notification Creation Tests

For the topic `domain.notifications.command.create_new_notification`

1. Repeat 1-3 of [Notification Interaction Tests] to set yourself up to test

2. Construct a notification message according to [these schemata](https://bitbucket.org/sofiinc/avro-schemas/src/master/notifications/schemas/notification-center/CreateNotificationRequest.avdl)
    * All properties are required, with the exception of `webUrl` and `nativePath` which may both be `null`

4. Run

```
kafka-avro-console-producer --bootstrap-server kraken-qa-[x]:9092 --property schema.registry.url=http://kraken-qa-[x]:8081 --topic domain.notifications.command.create_new_notification \

# for example, using kraken-dev-7:9092
./kafka-avro-console-producer --broker-list kraken-dev-7:9092 --property schema.registry.url=http://kraken-dev-7:8081 --property parse.key=true --property key.schema='{"type":"record","name":"PartyIdKey","namespace":"com.sofi.notifications.schemas","fields":[{"name":"customerId","type":"long"}],"version":1}' --property value.schema='{"type":"record","name":"CreateNotificationRequest","namespace":"com.sofi.notifications.schemas","fields":[{"name":"communicationId","type":{"type":"string","avro.java.string":"String"}},{"name":"customerId","type":"long"},{"name":"body","type":{"type":"string","avro.java.string":"String"}},{"name":"nativePath","type":["null",{"type":"string","avro.java.string":"String"}],"default":null},{"name":"webUrl","type":["null",{"type":"string","avro.java.string":"String"}],"default":null},{"name":"meta","type":{"type":"record","name":"Metadata","fields":[{"name":"origin","type":{"type":"string","avro.java.string":"String"}},{"name":"eventTime","type":{"type":"long","logicalType":"timestamp-millis"}},{"name":"urgency","type":"boolean"}]}}],"version":1}' --topic domain.notifications.command.create_new_notification
```

4. Observe the logs (`dc logs -f notifications) and the changing result of fetching a page of notifications (`/notifications`, as above)
    * Look for logs including "QA Test Engineer"
    * They should look like:

```
notifications                         | 2018-12-12 22:42:38,059 INFO  [:::] [NOTIFICATIONS-CENTER-NOTIFICATION-CREATION-945b1ce3-6a13-442d-bf94-f69f70ffca86-StreamThread-1] com.sofi.notifications.config.SpringKafkaConfig : Invoked KStream.foreach(id, notificationRequest), communicationId = [comms id fake], origin = [QA Test Engineer]
notifications                         | 2018-12-12 22:42:38,060 INFO  [:::] [NOTIFICATIONS-CENTER-NOTIFICATION-CREATION-945b1ce3-6a13-442d-bf94-f69f70ffca86-StreamThread-1] com.sofi.notifications.service.NotificationService : createNotification partyId = [12], communicationId = [comms id fake],
notifications                         | nativePath = [null], webUrl = [null],
notifications                         | eventDate = [1970-01-01T00:02:03.045Z], urgent = [true], origin = [QA Test Engineer]
notifications                         | 2018-12-12 22:42:38,060 INFO  [:::] [NOTIFICATIONS-CENTER-NOTIFICATION-CREATION-945b1ce3-6a13-442d-bf94-f69f70ffca86-StreamThread-1] com.sofi.notifications.db.NotificationRepository : NotificationRepository.createNotification(
notifications                         | partyId = [12], communicationId = [comms id fake], nativePath = [null],
notifications                         | webUrl = [null], eventDt = [1970-01-01T00:02:03.045Z], urgent = [true], origin = [QA Test Engineer])
notifications                         | 2018-12-12 22:42:38,066 INFO  [:::] [pool-1-thread-16] com.sofi.notifications.service.NotificationService : notification to be sent as event = [NotificationEntity[communicationId=comms id fake, eventDate=1970-01-01T00:02:03.045Z, message=qa message body, nativePath=null, notificationId=7d1bb5ad-bc95-41c8-9c8e-70b4476f8e73, origin=QA Test Engineer, partyId=12, status=UNSEEN, urgent=true, webUrl=null]]
notifications                         | 2018-12-12 22:42:38,066 INFO  [:::] [pool-1-thread-16] com.sofi.notifications.db.NotificationRepository : NotificationRepository.fetchUnseenNotificationsCount(partyId = [12])
notifications                         | 2018-12-12 22:42:38,068 INFO  [:::] [pool-1-thread-16] com.sofi.notifications.config.SpringKafkaConfig : Completed attempt to write notification to the database
notifications                         | 2018-12-12 22:42:38,068 INFO  [:::] [pool-1-thread-17] com.sofi.notifications.service.BadgeCountProducer : producing to TOPIC com.sofi.avro.schemas.AppBadgeCount: KEY {"partyId": 12}, VALUE {"partyId": 12, "badgeCount": 4}
```
